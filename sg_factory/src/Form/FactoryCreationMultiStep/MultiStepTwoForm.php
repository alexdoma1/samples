<?php

namespace Drupal\sg_factory\Form\FactoryCreationMultiStep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\sg_book\Form\CreateCarNodeModalForm;
use Drupal\node\Entity\Node;

/**
 * Class MultiStepTwoForm.
 */
class MultiStepTwoForm extends MultiStepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multistep_form_two';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    // Create an array to contain options to pass into field_factory_city.
    $field_factory_city_options = CreateCarNodeModalForm::getTaxonomyTermsListByName(SG_FACTORY_TAXONOMY_NAME_CITY);
    // Create an array to contain options to pass into field_factory_country.
    $field_factory_country_options = CreateCarNodeModalForm::getTaxonomyTermsListByName(SG_FACTORY_TAXONOMY_NAME_COUNTRY);

    $form['field_factory_city'] = [
      '#type' => 'select',
      '#title' => $this->t('City'),
      '#options' => $field_factory_city_options,
      '#default_value' => $this->store->get('field_factory_city') ? $this->store->get('field_factory_city') : '',
    ];

    $form['field_factory_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $field_factory_country_options,
      '#default_value' => $this->store->get('field_factory_country') ? $this->store->get('field_factory_country') : '',
    ];

    $form['actions']['previous'] = [
      '#type' => 'link',
      '#title' => $this->t('Previous'),
      '#attributes' => [
        'class' => ['button'],
      ],
      '#weight' => 0,
      '#url' => Url::fromRoute('sg_factory.factory_multistep_one'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('field_factory_city', $form_state->getValue('field_factory_city'));
    $this->store->set('field_factory_country', $form_state->getValue('field_factory_country'));

    // Save the data.
    self::saveData();
    $form_state->setRedirect('sg_factory.factory_multistep_one');
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function saveData() {
    $title = $this->store->get('title');
    $body = $this->store->get('body');
    $city = $this->store->get('field_factory_city');
    $country = $this->store->get('field_factory_country');
    if (!empty($title)) {
      $node = Node::create([
        'type' => 'factory',
        'title' => $title,
      ]);
      $node->set('body', $body);
      if ($city != '_none') {
        $node->field_factory_city->target_id = $city;
      }
      if ($country != '_none') {
        $node->field_factory_country->target_id = $country;
      }
      $node->save();
    }
    $this->deleteStore();
    drupal_set_message($this->t('The factory has been created.'));
  }

}
