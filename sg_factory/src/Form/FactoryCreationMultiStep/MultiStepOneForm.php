<?php

namespace Drupal\sg_factory\Form\FactoryCreationMultiStep;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class MultiStepOneForm.
 */
class MultiStepOneForm extends MultiStepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multistep_form_one';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Factory title'),
      '#default_value' => $this->store->get('title') ? $this->store->get('title') : '',
    ];

    $form['body'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Factory description'),
      '#default_value' => $this->store->get('body') ? $this->store->get('body') : '',
    ];

    $form['actions']['submit']['#value'] = $this->t('Next');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('title', $form_state->getValue('title'));
    $this->store->set('body', $form_state->getValue('body'));
    $form_state->setRedirect('sg_factory.factory_multistep_two');
  }

}
