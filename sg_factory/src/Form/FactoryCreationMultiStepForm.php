<?php

namespace Drupal\sg_factory\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sg_book\Form\CreateCarNodeModalForm;
use Drupal\node\Entity\Node;

/**
 * Class FactoryCreationMultiStepForm.
 *
 * @package Drupal\sg_factory\Form
 */
class FactoryCreationMultiStepForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'factory_creation_multistep_form';
  }

  /**
   * Returns a form wrapper id string.
   */
  public function getFormWrapperId() {
    return Html::getId($this->getFormId()) . '-wrapper';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();
    // Set storage default values.
    $storage = array_merge([
      'step' => 0,
      'title' => '',
      'body' => '',
      'field_factory_city' => '_none',
      'field_factory_country' => '_none',
    ], $storage);

    // Prepare form wrapper to be used lately by ajax
    // callbacks.
    $form = array_merge($form, [
      '#prefix' => new FormattableMarkup(
        '<div id="@id">',
        ['@id' => $this->getFormWrapperId()]
      ),
      '#suffix' => '</div>',
    ]);

    // Prepare actions container.
    $form['actions'] = [
      '#type' => 'container',
      '#weight' => 1000,
    ];

    // Render the form based on step value.
    switch ($storage['step']) {
      // Step 1.
      default:
      case 0:
        $form['title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Factory title'),
          '#default_value' => $storage['title'],
        ];

        $form['body'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Factory description'),
          '#default_value' => $storage['body'],
        ];

        $form['actions']['submit'] = [
          '#type' => 'button',
          '#step' => 1,
          '#value' => $this->t('Next'),
          '#action' => 'nextStep',
          '#ajax' => [
            'wrapper' => $this->getFormWrapperId(),
            'callback' => '::submitFormAjax',
          ],
        ];

        break;

      // Final step.
      case 1:
        // Create an array to contain options
        // to pass into field_factory_city.
        $field_factory_city_options = CreateCarNodeModalForm::getTaxonomyTermsListByName(SG_FACTORY_TAXONOMY_NAME_CITY);
        // Create an array to contain options
        // to pass into field_factory_country.
        $field_factory_country_options = CreateCarNodeModalForm::getTaxonomyTermsListByName(SG_FACTORY_TAXONOMY_NAME_COUNTRY);

        $form['field_factory_city'] = [
          '#type' => 'select',
          '#title' => $this->t('City'),
          '#options' => $field_factory_city_options,
          '#default_value' => $storage['field_factory_city'],
        ];

        $form['field_factory_country'] = [
          '#type' => 'select',
          '#title' => $this->t('Country'),
          '#options' => $field_factory_country_options,
          '#default_value' => $storage['field_factory_country'],
        ];

        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#step' => 'final',
          '#value' => $this->t('Create a factory'),
          '#action' => 'closeModal',
          '#ajax' => [
            'wrapper' => $this->getFormWrapperId(),
            'callback' => '::submitFormAjax',
          ],
        ];

        break;
    }

    // Prepare a link to a previous page.
    if ($storage['step']) {
      $form['back'] = [
        '#weight' => 999,
        '#type' => 'button',
        '#step' => 'back',
        '#value' => $this->t('Back'),
        '#action' => 'previousStep',
        '#ajax' => [
          'wrapper' => $this->getFormWrapperId(),
          'callback' => '::submitFormAjax',
        ],
      ];
    }

    // Make sure all the storage changes have been saved.
    $form_state->setStorage($storage);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $storage = $form_state->getStorage();
    $title = $storage['title'];
    $body = $storage['body'];
    $city = $form_state->getValue('field_factory_city');
    $country = $form_state->getValue('field_factory_country');
    if (!empty($title)) {
      $node = Node::create([
        'type' => 'factory',
        'title' => $title,
      ]);
      $node->set('body', $body);
      if ($city != '_none') {
        $node->field_factory_city->target_id = $city;
      }
      if ($country != '_none') {
        $node->field_factory_country->target_id = $country;
      }
      $node->save();
    }
  }

  /**
   * Implements ajax form submission.
   */
  public function submitFormAjax(array &$form, FormStateInterface $form_state) {

    $storage = $form_state->getStorage();
    $trigger = $form_state->getTriggeringElement();

    // If trigger element has step specified, change it.
    if (!empty($trigger['#step']) && $trigger['#step'] == 1) {
      $storage['title'] = $form_state->getValue('title');
      $storage['body'] = $form_state->getValue('body');
    }
    elseif (!empty($trigger['#step']) && $trigger['#step'] == 'back') {
      $storage['field_factory_city'] = $form_state->getValue('field_factory_city');
      $storage['field_factory_country'] = $form_state->getValue('field_factory_country');
    }

    // Clean form related values and prepare
    // state values to be used later on.
    $form_state->cleanValues();

    $ajax_response = new AjaxResponse();

    if ($trigger['#type'] == 'submit' &&
      $form_state->getErrors()) {
      return $form;
    }
    // If a specific existing method description is present w/in
    // the trigger element, execute it.
    if (!empty($trigger['#action']) &&
      method_exists($this, $trigger['#action'])) {
      $method_name = $trigger['#action'];
      $this->$method_name($form, $form_state, $storage, $ajax_response);
    }

    // Save form storage and rebuild the form itself using the updated state
    // in order to maintain it after the update is made.
    $form_state->setStorage($storage);
    $form = \Drupal::formBuilder()->rebuildForm($this->getFormId(), $form_state);

    $ajax_response->addCommand(new ReplaceCommand('#' . $this->getFormWrapperId(), $form));

    return $ajax_response;
  }

}
