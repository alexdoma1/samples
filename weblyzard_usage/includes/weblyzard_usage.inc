<?php

/**
 * @file
 * Weblyzard API integration module.
 *
 * Module to use weblyzard api call on cron run.
 */

/**
 * Custom function to populate and send data for weblyzard through cron run.
 */
function weblyzard_usage_api_call() {
  $api_data = weblyzard_usage_get_weblyzard_api_data();
  $discussions_ids = weblyzard_usage_get_discussions_ids_for_api();

  foreach ($discussions_ids as $discussion_id) {
    $document = node_load($discussion_id);
    $weblyzard_response = array();

    if (isset($document->field_weblyzard_api_response) && !empty($document->field_weblyzard_api_response)) {
      $weblyzard_response = unserialize($document->field_weblyzard_api_response[LANGUAGE_NONE][0]['value']);
    }

    $document_content = views_embed_view('weblyzard_docs', 'weblyzard_export', $discussion_id);

    $document_post = weblyzard_usage_post_udpate_weblyzard_document($document, $document_content, $api_data);
    if ($document_post) {
      $weblyzard_response = (array) $document_post;
    }

    $document->field_weblyzard_api_response[LANGUAGE_NONE][0]['value'] = serialize($weblyzard_response);
    node_save($document);
  }
}

/**
 * Function to get weblyzard api data.
 *
 * @return array
 *   repository_id - repository id for weblizard api
 *   server_url - server url for weblyzard api
 *   version - api version for weblyzard api
 *   api_auth - authentication function for weblyzard api
 */
function weblyzard_usage_get_weblyzard_api_data() {

  $repo_id = 'communidata.weblyzard.com/api';
  $api_username = 'api@communidata.weblyzard.com';
  $api_pass = '';
  $api_server_url = 'https://api.weblyzard.com';
  $api_version = '0.3';

  $token_command = '#!/bin/bash
curl -s -u ' . $api_username . ':' . $api_pass . ' ' . $api_server_url . '/' . $api_version . '/token';

  $token = shell_exec($token_command);

  $api_auth = 'Authorization: Bearer ' . $token;

  return array(
    'repository_id' => $repo_id,
    'server_url' => $api_server_url,
    'version' => $api_version,
    'api_auth' => $api_auth,
  );
}

/**
 * Function to get discussion ids.
 */
function weblyzard_usage_get_discussions_ids_for_api() {
  $enabled_emails = array(
    'XXXXX@XXXX.com',
  );
  $nodes = array();

  foreach ($enabled_emails as $enabled_email) {
    $weblyzard_user = user_load_by_mail($enabled_email);

    if ($weblyzard_user) {
      $query = db_select('node', 'n');
      $query->innerJoin('field_data_field_weblyzard_widget', 'fww', 'fww.entity_id=n.nid');
      $query->condition('n.type', 'document');
      $query->condition('n.uid', $weblyzard_user->uid);
      $query->condition('fww.field_weblyzard_widget_value', 1);
      $query->fields('n', array('nid'));
      $results = $query->execute()->fetchAll();

      if (!empty($results)) {
        foreach ($results as $result) {
          $nodes[] = $result->nid;
        }
      }
    }
  }

  return $nodes;
}

/**
 * Function to post/update document on weblyzard.
 */
function weblyzard_usage_post_udpate_weblyzard_document($document, $content, $api_data) {
  $json_document = weblyzard_usage_get_json_object_for_weblizard_api($document, $content);

  $document_post = FALSE;
  $previous_response = FALSE;
  if (isset($document->field_weblyzard_api_response) && !empty($document->field_weblyzard_api_response)) {
    $previous_response = weblyzard_usage_get_previous_weblyzard_response($document);
  }

  if ($previous_response) {
    $content_id = $previous_response['_id'];

    $document_post = weblyzard_usage_weblyzard_api_update_document($json_document, $api_data, $content_id);
  }

  return $document_post;
}

/**
 * Function to get json object for weblyzard api.
 */
function weblyzard_usage_get_json_object_for_weblizard_api($node, $content) {
  global $base_url;
  $account = user_load($node->uid);
  $reference_url = $base_url . '/document/' . $node->nid;
  $title = $node->title;
  $published_date = date('Y-m-d', $node->created) . 'T' . date('G:i:s', $node->created) . 'Z';

  $json_content_array = array(
    'content' => $content,
    'content_type' => "text/html",
    "repository_id" => "communidata.weblyzard.com/api",
    "uri" => $reference_url,
    "title" => $title,
    'meta_data' => array(
      'published_date' => $published_date,
      'user_id' => $account->uid,
      'user_name' => $account->name,
      'user_screen_name' => weblyzard_usage_get_user_full_name($account),
      'language_id' => user_preferred_language($account)->language,
    ),
  );

  $json_content = json_encode($json_content_array, JSON_UNESCAPED_SLASHES | JSON_HEX_APOS);

  return $json_content;
}

/**
 * Function to get an array of previous weblyzard response.
 *
 * @return bool|array
 *   an array of weblyzard post/update response.
 */
function weblyzard_usage_get_previous_weblyzard_response($document) {
  if ($document->type == 'document') {
    $previous_response = unserialize($document->field_weblyzard_api_response[LANGUAGE_NONE][0]['value']);

    return $previous_response;
  }

  return FALSE;
}

/**
 * Function to update document on weblyzard repository.
 *
 * @param string $json_content
 *   Json object string @see weblyzard_usage_get_json_object_for_weblizard_api.
 * @param array $api_data
 *   Api data - @see weblyzard_usage_get_weblyzard_api_data.
 * @param int $content_id
 *   Content id, as returned by POST query to weblyzard.
 *
 * @return object|bool
 *   Converted to php json object, returned from weblyzard api.
 *
 * @see https://api.weblyzard.com/doc/ui/#/
 */
function weblyzard_usage_weblyzard_api_update_document($json_content, array $api_data, $content_id) {
  $api_update_command = '#!/bin/bash
    curl -H "' . $api_data['api_auth'] . '" "Content-Type: application/json" -X PUT --data \''
    . $json_content . '\' ' . $api_data['server_url'] . '/' . $api_data['version']
    . '/documents/' . $api_data['repository_id'] . '/' . $content_id;

  $update_command_result_json = shell_exec($api_update_command);
  $update_command_result = json_decode($update_command_result_json);

  if (isset($update_command_result->error) && !isset($update_command_result->created)
    && !isset($update_command_result->updated)) {
    drupal_set_message('Some error occurred during getting file from weblyzard', 'error');

    return FALSE;
  }
  elseif (isset($update_command_result->updated) && $update_command_result->updated) {
    return $update_command_result;
  }

  return FALSE;
}

/**
 * Function to post document to weblyzard repository.
 *
 * @param string $json_content
 *   Json object string @see weblyzard_usage_get_json_object_for_weblizard_api.
 * @param array $api_data
 *   Array of all necessary data for accessing weblyzard API.
 *
 * @see weblyzard_usage_get_weblyzard_api_data
 *
 * @return mixed
 *   bool - in case of error.
 *   int - document id on weblyzard repository.
 */
function weblyzard_usage_weblyzard_api_post_document($json_content, array $api_data) {
  $api_post_command = '#!/bin/bash
curl -H "' . $api_data['api_auth'] . '" -H "Content-Type: application/json" -X POST --data \'' . $json_content . '\' ' . $api_data['server_url'] . '/' . $api_data['version'] . '/documents/' . $api_data['repository_id'];

  $post_command_result_json = shell_exec($api_post_command);
  $post_command_result = json_decode($post_command_result_json);

  if (isset($post_command_result->error) && !isset($post_command_result->created)) {
    drupal_set_message(t('Some error occurred during posting file to weblyzard'), 'error');

    return FALSE;
  }
  elseif (isset($post_command_result->created) && $post_command_result->created) {
    return $post_command_result;
  }
}

/**
 * Function to get users full name.
 */
function weblyzard_usage_get_user_full_name($obj_user) {
  $first_name = '';
  $last_name = '';

  if (isset($obj_user->field_first_name) && !empty($obj_user->field_first_name)) {
    $first_name = check_plain($obj_user->field_first_name[LANGUAGE_NONE][0]['value']);
  }

  if (isset($obj_user->field_last_name) && !empty($obj_user->field_last_name)) {
    $last_name = check_plain($obj_user->field_last_name[LANGUAGE_NONE][0]['value']);
  }

  $full_name = $first_name . ' ' . $last_name;
  if (mb_strlen($full_name) < 3 && empty($obj_user->field_first_name) && empty($obj_user->field_last_name)) {
    $full_name = check_plain($obj_user->name);
  }

  return $full_name;
}
