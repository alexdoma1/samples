<?php

namespace Drupal\unl_cart;

use Drupal\Core\Url;

/**
 * Utilty functions for cart.
 */
class CartHelper {

  /**
   * Get Storage session.
   */
  private static function getStorage() {
    $storage = new CartSession();
    return $storage;
  }

  /**
   * Function for cart retrieval.
   *
   * @param int $nid
   *   We are using the node id to store the node in the shopping cart.
   *
   * @return mixed
   *   Returning the cart contents.
   *   An empty array if there is nothing in the cart
   */
  public static function getCart($nid = NULL) {
    $storage = static::getStorage();
    return $storage->getCart($nid);
  }

  /**
   * Callback function for cart/remove/.
   *
   * @param int $nid
   *   We are using the node id to remove the node in the cart.
   */
  public static function removeFromCart($nid = NULL) {
    $nid = (int) $nid;
    $storage = static::getStorage();
    $storage->removeFromCart($nid);
  }

  /**
   * Cart reset.
   */
  public static function emptyCart() {
    $storage = static::getStorage();
    $storage->emptyCart();
  }

  /**
   * Add to cart.
   *
   * @param int $id
   *   Node id.
   * @param array $params
   *   Entity types.
   */
  public static function addToCart($id, array $params = []) {
    $storage = static::getStorage();
    $storage->addToCart($id, $params);
  }

  /**
   * Render function.
   *
   * @param string $template_name
   *   Name of the template.
   * @param string $variable
   *   Name of the unl cart.
   */
  public static function render(
    $template_name = 'unl-cart-cart-template.html.twig',
    $variable = NULL
  ) {
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate(drupal_get_path('module',
        'unl_cart') . '/templates/' . $template_name);
    return $template->render(['unl_cart' => $variable ? $variable : self::getCartData()]);
  }

  /**
   * Get Cart data.
   */
  public static function getCartData() {
    $config = self::cartSettings();
    $cart = self::getCart();

    $basic_cart = [];

    if (empty($cart['cart'])) {
      $basic_cart['empty']['text'] = $config->get('empty_cart');
      $basic_cart['empty']['status'] = TRUE;
    }
    else {
      if (is_array($cart['cart']) && count($cart['cart']) >= 1) {

        foreach ($cart['cart'] as $nid => $node) {
          $title = $node->get('title')->getValue();;
          $url_remove = new Url('unl_cart.cartremove', ["nid" => $nid]);
          $basic_cart['data']['contents'][$nid] = [
            'developer_name' => $title[0]['value'],
            'remove_url' => $url_remove,
          ];
        }
        $basic_cart['config']['view_cart_button'] = $config->get('view_cart_button') . ' (' . count($cart['cart']) . ')';
        $basic_cart['empty']['status'] = FALSE;
      }
    }

    return $basic_cart;
  }

  /**
   * Geting prefix data.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array
   *   Basic cart.
   */
  public static function getCartPrefixData($nid) {
    $url = new Url('unl_cart.cartremove', ["nid" => $nid]);
    $cart = CartHelper::getCart($nid);
    $basic_cart = [];
    $basic_cart['delete_url'] = $url->toString();
    $basic_cart['notempty'] = FALSE;
    if (!empty($cart['cart'])) {
      $basic_cart['notempty'] = TRUE;
      $title = $cart['cart']->get('title')->getValue()[0]['value'];
      $basic_cart['title'] = $title;
    }

    return $basic_cart;
  }

  /**
   * Get the cart configuration.
   *
   * @return object
   *   Array of settings
   */
  public static function cartSettings() {
    $return = \Drupal::config('unl_cart.settings');
    return $return;
  }

}
