<?php

namespace Drupal\unl_cart\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\unl_cart\CartHelper;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Contains the cart controller.
 */
class CartController extends ControllerBase {

  /**
   * Cart Page.
   *
   * @return array
   *   Returns Drupal cart form or null
   */
  public function cart() {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $helper = new CartHelper();
    $cart = $helper::getCart();
    $config = $helper::cartSettings();
    if (!empty($cart['cart'])) {
      return \Drupal::formBuilder()
        ->getForm('\Drupal\unl_forms\Form\CartForm');
    }
    return [
      '#type' => 'markup',
      '#markup' => t('@empty_cart', array(@empty_cart => $config->get('empty_cart'))),
    ];

  }

  /**
   * Remove node from cart.
   *
   * @param int $nid
   *   Node id of the cart content.
   *
   * @return Object|string
   *   Redirect to HTTP_REFERER
   */
  public function removeFromCart($nid) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    CartHelper::removeFromCart($nid);
    $response = new \stdClass();
    $response->status = TRUE;
    $response->block = CartHelper::render();
    return new JsonResponse($response);
  }

  /**
   * Add node to cart.
   *
   * @param int $nid
   *   Node id of the cart content.
   *
   * @return Object|string
   *   Json Object response with html div text   *    */
  public function addToCart($nid) {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $param['entitytype'] = "node";
    CartHelper::addToCart($nid, $param);
    $response = new \stdClass();
    $response->status = TRUE;
    $response->block = CartHelper::render();
    return new JsonResponse($response);

  }

}
