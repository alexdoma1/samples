<?php

namespace Drupal\unl_cart\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\unl_cart\CartHelper;

/**
 * Provides a 'UNL Cart' block.
 *
 * @Block(
 *   id = "unl_cart_cartblock",
 *   admin_label = @Translation("Cart Block")
 * )
 */
class CartBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#markup' => CartHelper::render(),
      '#cache' => ['max-age' => 0],
    ];
  }

}
