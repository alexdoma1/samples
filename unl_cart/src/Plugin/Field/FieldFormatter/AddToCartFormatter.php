<?php

namespace Drupal\unl_cart\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'developer_add_to_cart_field' formatter.
 *
 * @FieldFormatter(
 *   id = "DeveloperAddToCartFormatter",
 *   label = @Translation("Add to cart"),
 *   field_types = {
 *     "developer_add_to_cart_field"
 *   }
 * )
 */
class AddtoCartFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getEntity();

    $config = \Drupal::config('unl_cart.settings');
    $elements = [];
    $option = [
      'query' => ['entitytype' => $entity->getEntityTypeId()],
      'absolute' => TRUE,
    ];

    $url = Url::fromRoute('unl_cart.cartadd', ["nid" => $entity->id()], $option);
    $link = '<a class="cart_link_button_ajax btn" href="' . $url->toString() . '">'
      . $this->t('@config_adding', array(@config_adding => $config->get('add_to_cart_button'))) . '</a>';

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => '<div class="btn-container">' . $link . '</div>',
      ];
    }

    $elements['#attached']['library'][] = 'core/drupal.ajax';
    return $elements;
  }

}
