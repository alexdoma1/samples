<?php

namespace Drupal\unl_cart\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'developer_add_to_cart_field' widget.
 *
 * @FieldWidget(
 *   id = "DeveloperAddToCartWidget",
 *   label = @Translation("Add to cart"),
 *   field_types = {
 *     "developer_add_to_cart_field"
 *   }
 * )
 */
class AddToCartWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element += [
      '#type' => 'checkbox',
      '#default_value' => 1,
      '#value' => 1,
      '#size' => 1,
      '#maxlength' => 1,
    ];
    return ['value' => $element];
  }

}
