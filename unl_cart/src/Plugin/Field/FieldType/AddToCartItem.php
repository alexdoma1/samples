<?php

namespace Drupal\unl_cart\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\unl_cart\CartHelper;

/**
 * Plugin implementation of the 'developer_add_to_cart_field' field type.
 *
 * @FieldType(
 *   id = "developer_add_to_cart_field",
 *   label = @Translation("Add to cart"),
 *   description = @Translation("Demonstrates a field."),
 *   category = @Translation("UNL"),
 *   default_widget = "DeveloperAddToCartWidget",
 *   default_formatter = "DeveloperAddToCartFormatter",
 * )
 */
class AddToCartItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $config = CartHelper::cartSettings();
    $properties['value'] = DataDefinition::create('boolean')
      ->setLabel(t('@add_to_cart', array(@add_to_cart => $config->get('add_to_cart_button'))));
    return $properties;
  }

}
