<?php

namespace Drupal\unl_cart;

/**
 * Class CartSession.
 */
class CartSession implements CartInterface {

  /**
   * Function for cart retrieval.
   *
   * @param int $nid
   *   We are using the node id to store the node in the cart.
   *
   * @return mixed
   *   Returning the cart contents.
   *   An empty array if there is nothing in the cart
   */
  public function getCart($nid = NULL) {

    if (isset($nid)) {
      return [
        "cart" => $_SESSION['unl_cart']['cart'][$nid],
      ];
    }
    if (isset($_SESSION['unl_cart']['cart'])) {
      return [
        "cart" => $_SESSION['unl_cart']['cart'],
      ];
    }
    // Empty cart.
    return ["cart" => []];
  }

  /**
   * Callback function for cart/remove/.
   *
   * @param int $nid
   *   We are using the node id to remove the node in the cart.
   */
  public function removeFromCart($nid) {
    $nid = (int) $nid;
    if ($nid > 0) {
      unset($_SESSION['unl_cart']['cart'][$nid]);
    }
  }

  /**
   * Shopping cart reset.
   */
  public function emptyCart() {
    unset($_SESSION['unl_cart']['cart']);
  }

  /**
   * Add to cart.
   *
   * @param int $id
   *   Node id.
   * @param array $params
   *   Entity type.
   */
  public function addToCart($id, array $params = []) {
    if (!empty($params)) {
      $entitytype = $params['entitytype'];
      $entity = \Drupal::entityTypeManager()
        ->getStorage($entitytype)
        ->load($id);
      $_SESSION['unl_cart']['cart'][$id] = $entity;
    }
  }

}
