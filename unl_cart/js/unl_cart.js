/**
 * @file
 * Contains js for the accordion example.
 * @prop drupalSettings
 * @prop result.block
 */

(function ($) {

    Drupal.behaviors.corpCart = {
        attach: function () {
            $(document).ready(function () {
                function countDevelopers() {
                    var count = 0;
                    if ($('.cart-box.developer-rows').length !== 0) {
                        var requestDevelopers = $('.request-developers');
                        requestDevelopers.show();
                        count = $('.cart-box .developer-rows').length;
                        requestDevelopers.text('request developers (' + count + ')');
                    } else $('.request-developers').text('request developers').hide();
                }
                countDevelopers();

                $('body').once().on('click', '.cart_link_button_ajax', function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: this.href,
                        success: function (result) {
                            $('.cart-box').html(result.block);
                            $('.form-cart-box').html(result.block);
                            Drupal.behaviors.AJAX.attach(document, drupalSettings);
                            $(".form-cart-box div.btn-container").remove();
                            if ($('.form-cart-box.developer-rows').length === 0) {
                                $('.form-close').click();
                            }
                            countDevelopers();
                        }
                    });

                });

            });
        }
    };

})(jQuery);
