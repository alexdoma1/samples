<?php

/**
 * @file
 * Discuto statistics module.
 *
 * Module counts necessary statistics, creates csv file and link to download it.
 */

/**
 * Builds form to download statistics csv file.
 */
function dis_statistics_download_statistics_form($form, $form_state) {

  $form['choose_statistics'] = array(
    '#type' => 'radios',
    '#title' => t('Choose statistics'),
    '#description' => t('Choose which of the statistics file to download'),
    '#options' => array(
      'users' => t('Users table'),
      'contributions' => t('Contributions table'),
      'emails' => t('Email activity table'),
      'elements' => t('Elements table'),
    ),
  );

  if (isset($_SESSION['file_to_download']) && $_SESSION['file_to_download']
    && isset($_SESSION['file_type'])) {

    $form['created_file'] = array(
      '#type' => 'item',
      '#title' => t('Download file'),
      '#markup' => l(t('click here to download file'), file_create_url('public://' . $_SESSION['file_type'] . '_statistics' . date('m-d-Y') . '.csv')),
    );

    unset($_SESSION['file_type']);
    unset($_SESSION['file_to_download']);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Download'),
  );

  return $form;
}

/**
 * Submit handler for statistics download form.
 */
function dis_statistics_download_statistics_form_submit($form, $form_state) {
  $chosen_data = $form_state['values']['choose_statistics'];

  switch ($chosen_data) {
    case 'users':
      $type = 'users';
      $file_name = 'public://' . $type . '_statistics' . date('m-d-Y') . '.csv';
      $batch_title = t('Downloading users info');
      $data = dis_statistics_get_active_users();

      break;

    case 'contributions':
      $type = 'contributions';
      $file_name = 'public://' . $type . '_statistics' . date('m-d-Y') . '.csv';
      $batch_title = t('Downloading contributions info');
      $data = dis_statistics_get_contributions_data();

      break;

    case 'emails':
      $type = 'emails';
      $file_name = 'public://' . $type . '_statistics' . date('m-d-Y') . '.csv';
      $batch_title = t('Downloading emails info');
      $data = dis_statistics_get_emails_data();

      break;

    case 'elements':
      $type = 'elements';
      $file_name = 'public://' . $type . '_statistics' . date('m-d-Y') . '.csv';
      $batch_title = t('Downloading elements info');
      $data = dis_statistics_get_elements_data();

      break;

    default:
      $type = 'users';
      $file_name = 'public://' . $type . '_statistics' . date('m-d-Y') . '.csv';
      $batch_title = t('Downloading users info');
      $data = dis_statistics_get_active_users();

      break;
  }

  $_SESSION['file_type'] = $type;

  // Delete file if it exists.
  if (file_exists($file_name)) {
    unlink($file_name);
  }

  // Create file and write headers to it.
  $csv_file = fopen($file_name, 'w') or die(t('Cant open file!'));

  $operations = array();

  while ($data) {
    $data_part = array_splice($data, 0, 50);
    $operations[] = array(
      'dis_statistics_download_csv_file_batch',
      array($data_part, $type),
    );
  }

  $batch = array(
    'title' => $batch_title,
    'operations' => $operations,
    'finished' => 'dis_statistics_download_csv_file_batch_finished',
  );
  batch_set($batch);
  batch_process();
}
