<?php

/**
 * @file
 * Discuto statistics module.
 *
 * Module counts necessary statistics, creates csv file and link to download it.
 */

/**
 * Batch process callback for getting users statistics.
 */
function dis_statistics_download_csv_file_batch($data_parts, $type, &$context) {
  $file_name = 'public://' . $type . '_statistics' . date('m-d-Y') . '.csv';

  $csv_file = fopen($file_name, 'a+') or die(t('Cant open file!'));

  foreach ($data_parts as $data_part) {
    $data_array = array();
    if ($type == 'users') {
      $data_array = dis_statistics_get_users_data_array_for_batch($data_part);
    }
    elseif ($type == 'contributions') {
      $data_array = dis_statistics_get_contributions_data_array_for_batch($data_part);
    }
    elseif ($type == 'emails') {
      $data_array = dis_statistics_get_emails_data_array_for_batch($data_part, $csv_file);
      if ($data_array === TRUE || $data_array == FALSE) {
        continue;
      }
    }
    elseif ($type == 'elements') {
      $data_array = dis_statistics_get_elements_data_array_for_batch($data_part);
    }
    $added_row = fputcsv($csv_file, $data_array, ',', '"');

    if ($added_row) {
      $context['results']['valid'][] = $data_array[0];
      $context['message'] = t('Adding datasets to file');
    }
    else {
      $context['results']['invalid'][] = $data_array[0];
    }
  }

  fclose($csv_file);
}

/**
 * Callback function for batch process finished.
 */
function dis_statistics_download_csv_file_batch_finished($success, $results, $operations) {
  if ($success) {

    if (!empty($results['invalid'])) {
      $message = 'Invalid data provied :';

      foreach ($results['invalid'] as $row) {
        $message .= '<br>' . $row;
      }
      drupal_set_message($message);
    }
    $_SESSION['file_to_download'] = TRUE;

    drupal_set_message(
      t('Now you can download file')
    );
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(
      t('An error occurred while processing @operation with arguments : @args',
        array(
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        )
      )
    );
  }
}
