<?php

/**
 * @file
 * Discuto statistics module.
 *
 * Module counts necessary statistics, creates csv file and link to download it.
 */

/**
 * Function to get active users.
 */
function dis_statistics_get_active_users() {
  $query = db_select('users', 'u');
  $query->condition('u.status', DIS_STATISTICS_USER_ACTIVE);
  $query->condition('u.uid', array(1, 0), 'NOT IN');
  $query->fields('u', array('uid'));
  $active_users = $query->execute()->fetchAll();

  return $active_users;
}

/**
 * Function to get number of discussions and ideations created by user.
 */
function dis_statistics_get_users_created_elements($user_id) {

  $query = db_select('node', 'n');
  $query->condition('n.type', 'consultation');
  $query->condition('n.uid', $user_id);
  $query->fields('n', array('nid'));
  $query->condition('n.title', db_like('sys_unfinished') . '%', 'NOT LIKE');
  $number_of_elements = $query->distinct()->execute()->rowCount();

  return $number_of_elements;
}

/**
 * Function to get number of discussions and ideations that user as joined.
 */
function dis_statistics_get_users_joined_elements($user_id) {
  $joined_role_name = CONSULTATION_JOINED_ROLE;
  $role_id = consultation_get_og_roleid_fromname($joined_role_name);

  $query = db_select('og_users_roles', 'roles');
  $query->condition('roles.rid', $role_id);
  $query->condition('roles.uid', $user_id);
  $query->fields('roles', array('gid'));
  $number_of_joined_elements = $query->distinct()->execute()->rowCount();

  return $number_of_joined_elements;
}

/**
 * Function to get number of user's contributions.
 */
function dis_statistics_get_users_contributions($user_id) {
  $number_of_contributions = 0;

  $query = db_select('cbased_users', 'cu');
  $query->condition('cu.uid', $user_id);
  $query->fields('cu', array('comments_count', 'votes_count'));
  $result = $query->execute()->fetchAll();

  if (!empty($result)) {
    foreach ($result as $contribution) {
      $number_of_contributions = $number_of_contributions + $contribution->comments_count + $contribution->votes_count;
    }
  }

  return $number_of_contributions;
}

/**
 * Function to get users other interactions.
 *
 * It is the sum of:
 *  # of uploaded files,
 *  # of added info pages,
 *  # of invited users,
 *  # of elements created (part of $sum_of_measured_interactions),
 *  # of elements joined (part of $sum_of_measured_interactions),
 *  # of contributions (part of $sum_of_measured_interactions).
 */
function dis_statistics_get_users_other_interactions($user_id, $sum_of_measured_interactions = 0) {

  $uploaded_files_number = dis_statistics_get_users_uploaded_files_number($user_id);
  $info_pages_number = dis_statistics_get_users_created_info_pages_number($user_id);
  $invited_users_number = dis_statistics_get_users_invited_users_number($user_id);

  $number_of_other_interactions = $uploaded_files_number + $info_pages_number
    + $invited_users_number + $sum_of_measured_interactions;

  return $number_of_other_interactions;
}

/**
 * Function to get number of user's uploaded files.
 */
function dis_statistics_get_users_uploaded_files_number($user_id) {

  $query_uploaded_files = db_select('file_managed', 'fm');
  $query_uploaded_files->condition('fm.uid', $user_id);
  $query_uploaded_files->condition('fm.status', FILE_STATUS_PERMANENT);
  $query_uploaded_files->fields('fm', array('fid'));
  $result_uploaded_files = $query_uploaded_files->distinct()->execute()->rowCount();

  return $result_uploaded_files;
}

/**
 * Function to get number of user's created info pages.
 */
function dis_statistics_get_users_created_info_pages_number($user_id) {

  $query_info_pages = db_select('node', 'n');
  $query_info_pages->condition('n.uid', $user_id);
  $query_info_pages->condition('n.type', 'info_page');
  $query_info_pages->fields('n', array('nid'));
  $result_info_pages = $query_info_pages->distinct()->execute()->rowCount();

  return $result_info_pages;
}

/**
 * Function to get number of user's invited users.
 */
function dis_statistics_get_users_invited_users_number($user_id) {
  $invited_users_number = 0;

  $query_invited_users = db_select('cbased_mail', 'cm');
  $query_invited_users->condition('cm.sender_uid', $user_id);
  $query_invited_users->fields('cm', array('r_number'));
  $result_invited_users = $query_invited_users->execute()->fetchAll();

  if (!empty($result_invited_users)) {
    foreach ($result_invited_users as $result_invited_mails) {
      $invited_users_number = $invited_users_number + $result_invited_mails->r_number;
    }
  }

  return $invited_users_number;
}

/**
 * Function to get contributions data for batch.
 */
function dis_statistics_get_contributions_data() {
  // Timestamp is Jan 1st 2017.
  $timestamp = strtotime('01 Jan 2017');

  $query_comments = db_select('comment', 'c');
  $query_comments->condition('c.status', COMMENT_PUBLISHED);
  $query_comments->condition('c.created', $timestamp, '>');
  $query_comments->condition('c.uid', array(1, 0), 'NOT IN');
  $query_comments->fields('c', array('cid'));
  $comments = $query_comments->distinct()->execute()->fetchAll();

  $query_votes = db_select('votingapi_vote', 'v');
  $query_votes->condition('v.uid', array(1, 0), 'NOT IN');
  $query_votes->condition('v.timestamp', $timestamp, '>');
  $query_votes->condition('v.tag', array('cbased-state_removed'), 'NOT IN');
  $query_votes->condition('v.value', array(0), 'NOT IN');
  $query_votes->fields('v', array('vote_id', 'entity_id', 'uid', 'timestamp'));
  $votes = $query_votes->distinct()->execute()->fetchAll();

  $contributions = array_merge_recursive($comments, $votes);

  return $contributions;
}

/**
 * Function to get email data for batch.
 */
function dis_statistics_get_emails_data() {
  // Timestamp is Jan 1st 2017.
  $timestamp = strtotime('01 Jan 2017');

  $query = db_select('cbased_mail', 'cm');
  $query->condition('cm.sender_uid', array(1, 0), 'NOT IN');
  $query->condition('cm.time', $timestamp, '>');
  $query->fields('cm', array(
    'sender_uid',
    'Subject',
    'recipients',
    'time',
    'mid',
  ));
  $emails = $query->distinct()->execute()->fetchAll();

  return $emails;
}

/**
 * Function to get elements data for batch.
 */
function dis_statistics_get_elements_data() {
  // Timestamp is Jan 1st 2017.
  $timestamp = strtotime('01 Jan 2017');

  $query = db_select('node', 'n');
  $query->condition('n.created', $timestamp, '>');
  $query->condition('n.type', 'consultation');
  $query->condition('n.title', db_like('sys_unfinished') . '%', 'NOT LIKE');
  $query->fields('n', array('nid'));
  $elements = $query->distinct()->execute()->fetchAll();

  return $elements;
}

/**
 * Function to get array of data for batch operation.
 */
function dis_statistics_get_users_data_array_for_batch($data_part) {
  $active_user = user_load($data_part->uid);
  $user_name = consultation_get_user_full_name($active_user);
  $user_mail = $active_user->mail;
  $user_id = $active_user->uid;

  $user_company = 'none';
  if (isset($active_user->field_company_affiliation) && !empty($active_user->field_company_affiliation)) {
    $user_company = $active_user->field_company_affiliation[LANGUAGE_NONE][0]['value'];
  }

  $user_creation_date = date('Y-m-d', $active_user->created);
  $last_sign_up = date('Y-m-d', $active_user->access);
  $created_elements = dis_statistics_get_users_created_elements($user_id);
  $joined_elements = dis_statistics_get_users_joined_elements($user_id);
  $contributions_number = dis_statistics_get_users_contributions($user_id);
  $sum_for_other_interactions = $created_elements + $joined_elements + $contributions_number;
  $other_interactions = dis_statistics_get_users_other_interactions($user_id, $sum_for_other_interactions);

  $data_array = array(
    $user_name,
    $user_mail,
    $user_id,
    $user_company,
    $user_creation_date,
    $last_sign_up,
    $created_elements,
    $joined_elements,
    $contributions_number,
    $other_interactions,
  );

  return $data_array;
}

/**
 * Function to get array of data for batch operation.
 */
function dis_statistics_get_contributions_data_array_for_batch($data_part) {
  if (isset($data_part->cid)) {
    $contribution_id = $data_part->cid;
    $contribution_type = 'comment';

    $comment_obj = comment_load($data_part->cid);
    $element_associated = $comment_obj->nid;
    if (isset($comment_obj->pid) && $comment_obj->pid != 0) {
      $element_associated = $comment_obj->pid;
    }

    $user_id = $comment_obj->uid;
    $user_obj = user_load($user_id);
    $user_name = consultation_get_user_full_name($user_obj);
    $user_mail = $user_obj->mail;
    $contribution_date = date('m-d-Y H:i', $comment_obj->created);
  }
  elseif (isset($data_part->vote_id)) {
    $contribution_type = 'vote';
    $contribution_id = $data_part->vote_id;
    $element_associated = $data_part->entity_id;
    $user_id = $data_part->uid;
    $user_obj = user_load($user_id);
    if ($user_obj) {
      $user_name = consultation_get_user_full_name($user_obj);
      $user_mail = $user_obj->mail;
    }
    else {
      $user_name = t('not set ot user was deleted');
      $user_mail = t('not set ot user was deleted');
    }
    $contribution_date = date('m-d-Y H:i', $data_part->timestamp);
  }

  $data_array = array(
    $contribution_id,
    $contribution_type,
    $element_associated,
    $user_id,
    $user_name,
    $user_mail,
    $contribution_date,
  );

  return $data_array;
}

/**
 * Function to get array of data for batch operation.
 */
function dis_statistics_get_emails_data_array_for_batch($data_part, $file) {
  $recipients = unserialize($data_part->recipients);
  $sender_user = user_load($data_part->sender_uid);
  if ($sender_user) {
    $sender_mail = $sender_user->mail;
    $send_date = date('m-d-Y H:i', $data_part->time);
    $email_subject = $data_part->Subject;
    $email_id = $data_part->mid;

    $recipients_number = count($recipients);

    if ($recipients_number > 1) {
      foreach ($recipients as $recipient) {
        $data_array = array(
          $email_id,
          $sender_mail,
          $recipient,
          $send_date,
          $email_subject,
        );

        fputcsv($file, $data_array, ',', '"');
      }

      return TRUE;
    }
    elseif ($recipients_number === 1) {
      $data_array = array(
        $email_id,
        $sender_mail,
        reset($recipients),
        $send_date,
        $email_subject,
      );
    }
    else {
      return FALSE;
    }
    return $data_array;

  }

  return FALSE;
}

/**
 * Function to get array of data for batch operation.
 */
function dis_statistics_get_elements_data_array_for_batch($data_part) {
  global $base_url;
  $element_id = $data_part->nid;
  $element_node = node_load($element_id);
  $element_title = $element_node->title;
  $published_date = date('m-d-Y H:i', $element_node->created);
  $closing_date = $element_node->field_valid[LANGUAGE_NONE][0]['value'];

  $element_type = t('Discussion');
  if ($element_node->field_idea_generating[LANGUAGE_NONE][0]['value'] == CONSULTATION_IS_IDEA) {
    $element_type = t('Ideation');
  }

  $visibility_type = t('Public');
  $visibility = $element_node->field_visibility[LANGUAGE_NONE][0]['value'];
  if ($visibility == CONSULTATION_IS_PRIVATE) {
    $visibility_type = t('Private');
  }

  $gid = consultation_get_gid_from_child('consultation', $element_id);
  $contributors_data = consultation_users_info($gid);
  $contributors_number = $contributors_data['contributors_data_count'];
  $invited_users = dis_statistics_get_invited_users_num($gid);
  $invited_joined_users_num = dis_statistics_get_invited_joined_users_num($gid);
  $contributions_number = dis_statistics_get_element_contributions_num($gid);
  $active_contributors = dis_statistics_get_active_contributed_users_num($gid);
  $element_link = $base_url . '/consultation/' . $gid;

  $data_array = array(
    $element_id,
    $element_title,
    $element_link,
    $element_type,
    $published_date,
    $visibility_type,
    $closing_date,
    $contributors_number,
    $invited_users,
    $invited_joined_users_num,
    $contributions_number,
    $active_contributors,
  );

  return $data_array;
}

/**
 * Function to get number of users invited to the discussion.
 */
function dis_statistics_get_invited_users_num($gid) {
  // Query to get number of invited users.
  $query = db_select(TABLE_CBASED_MAIL, 'invites');
  $query->condition('invites.gid', $gid);
  $query->fields('invites', array('r_number'));
  $result = $query->execute()->fetchAll();

  $number_of_invites = 0;
  if (!empty($result)) {
    foreach ($result as $item) {
      $number_of_invites = $number_of_invites + $item->r_number;
    }
  }

  return $number_of_invites;
}

/**
 * Function to get number of users, that were joined by invites.
 */
function dis_statistics_get_invited_joined_users_num($gid) {
  // Query to get invited users emails.
  $query = db_select(TABLE_CBASED_MAIL, 'invites');
  $query->condition('invites.gid', $gid);
  $query->fields('invites', array('recipients'));
  $result = $query->execute()->fetchAll();

  $recipients_array = array();
  if (!empty($result)) {
    foreach ($result as $item) {
      $recipients = unserialize($item->recipients);
      if (count($recipients) > 1) {
        foreach ($recipients as $recipient) {
          $recipients_array[] = $recipient;
        }
      }
      else {
        $recipients_array[] = reset($recipients);
      }
    }

    if (!empty($recipients_array)) {
      $recipients_array = array_unique($recipients_array);
    }
  }

  $invited_joined_users = 0;
  foreach ($recipients_array as $recipient_mail) {
    $invited_user_obj = user_load_by_mail($recipient_mail);
    if ($invited_user_obj) {
      $invited_joined_users++;
    }
  }

  return $invited_joined_users;
}

/**
 * Function to get number of contributions done on element.
 */
function dis_statistics_get_element_contributions_num($gid) {
  $comments_num = count(consultation_get_comments($gid));
  $votes_num = count(consultation_get_votes($gid));

  $contributions_number = $comments_num + $votes_num;

  return $contributions_number;
}

/**
 * Function to get number of active contributed users.
 */
function dis_statistics_get_active_contributed_users_num($gid) {
  $participants = og_get_users_by_roles($gid, array(consultation_get_og_roleid_fromname(CONSULTATION_JOINED_ROLE), consultation_get_og_roleid_fromname(OG_ADMINISTRATOR_ROLE)));
  $members = og_get_users_by_roles($gid, array(consultation_get_og_roleid_fromname(CONSULTATION_INVITED_ROLE)));
  $discussion_users = array_merge_recursive($participants, $members);

  $active_contributed_users = 0;
  if (!empty($discussion_users)) {
    $unique_discussion_users = array();
    foreach ($discussion_users as $discussion_user) {
      $unique_discussion_users[] = $discussion_user->uid;
    }

    $unique_discussion_users = array_unique($unique_discussion_users);
    foreach ($unique_discussion_users as $unique_user) {
      $user_comments_num = 0;
      $user_comments = consultation_get_user_comments($gid, $unique_user);
      if ($user_comments) {
        $user_comments_num = count($user_comments);
      }
      $user_votes = dis_statistics_get_element_user_votes($gid, $unique_user);
      $user_contibutions_sum = $user_comments_num + $user_votes;

      if ($user_contibutions_sum >= 2) {
        $active_contributed_users++;
      }
    }
  }

  return $active_contributed_users;
}

/**
 * Function to get number of user votes on discussion.
 */
function dis_statistics_get_element_user_votes($gid, $uid) {
  $pids = consultation_get_paragraph_nids($gid);
  $user_discussion_votes = 0;

  if (!empty($pids)) {
    $query = db_select('votingapi_vote', 'v');
    $query->condition('v.entity_id', $pids);
    $query->condition('v.uid', $uid);
    $query->condition('value', 0, '!=');
    $query->fields('v', array('vote_id'));
    $user_discussion_votes = $query->execute()->rowCount();
  }

  return $user_discussion_votes;
}
