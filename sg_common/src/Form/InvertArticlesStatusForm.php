<?php

namespace Drupal\sg_common\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class InvertArticlesStatusForm.
 *
 * @package Drupal\sg_common\Form
 */
class InvertArticlesStatusForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'invert_articles_status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['invert_articles_status'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Invert Articles Status'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'article')
      ->execute();

    $batch = array(
      'title' => t('Inverting of articles status...'),
      'operations' => array(
        array(
          '\Drupal\sg_common\InvertArticlesStatus::invertStatus',
          array($nids),
        ),
      ),
      'finished' => '\Drupal\sg_common\InvertArticlesStatus::invertStatusFinishedCallback',
    );
    batch_set($batch);
  }

}
