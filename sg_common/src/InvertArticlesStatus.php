<?php

namespace Drupal\sg_common;

use Drupal\node\Entity\Node;

/**
 * Class InvertArticlesStatus.
 */
class InvertArticlesStatus {

  /**
   * Function to invert articles status.
   */
  public static function invertStatus($nids, &$context) {

    // Materials count that should be processed as a batch for one time.
    $limit = 1;
    // Set a default values for batch.
    if (empty($context['sandbox']['progress'])) {
      // Current number of the processed data.
      $context['sandbox']['progress'] = 0;
      // Total number of data to process.
      $context['sandbox']['max'] = count($nids);
    }
    // Create a batch variable to contain the articles nids array.
    if (empty($context['sandbox']['nids'])) {
      $context['sandbox']['nids'] = $nids;
    }

    $counter = 0;
    if (!empty($context['sandbox']['nids'])) {
      // Remove the already precessed data from array.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['nids'], 0, $limit);
      }
    }
    foreach ($context['sandbox']['nids'] as $nid) {
      if ($counter != $limit) {
        $node = Node::load($nid);
        if ($node->status->value == TRUE) {
          $node->setPublished(FALSE);
        }
        else {
          $node->setPublished(TRUE);
        }
        $node->save();
        $counter++;
        $context['sandbox']['progress']++;
        $context['message'] = t('Now processing article %progress of %count', array('%progress' => $context['sandbox']['progress'], '%count' => $context['sandbox']['max']));
        $context['results']['processed'] = $context['sandbox']['progress'];
      }
    }
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Function to be called after articles were processed.
   */
  public function invertStatusFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        $results['processed'],
        'One article processed.', '@count articles processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

}
