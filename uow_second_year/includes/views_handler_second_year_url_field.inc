<?php

/**
 * @file
 * Custom views handler definition.
 *
 */

/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_second_year_url_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }

  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Render callback handler.
   *
   * Return the markup that will appear in the rendered field.
   */
  function render($values) {
    if (!empty($values->entity)) {
      if ($values->entity->type === UOW_CORE_CT_COURSE) {

        $course_tid        = uow_core_field_get_item('node', $values->entity,
          'field_subject', 0, 'target_id');
        $field_course_code = uow_core_field_get_item('node', $values->entity,
          'field_course_code', 0, 'value');

        $results = views_get_view_result(UOW_IMPORT_QUERY_COURSE_LANDING_PAGE_BY_COURSE_COURSE_CODE,
          'default', $field_course_code);

        if (!empty($results)) {
          $landing_page = array_shift($results);
          $path         = 'node/' . $landing_page->nid . '/' . $course_tid;

          $landing_page_node = node_load($landing_page->nid);
          $course_data_nid   = uow_core_field_get_item('node',
            $landing_page_node,
            'field_course_data', UOW_IMPORT_INDEX_COURSE_NEXT_YEAR,
            'target_id');

          if (isset($course_data_nid) && $course_data_nid == $values->entity->nid) {
            $path .= '?next-year=1';
          }
          return $path;
        }

      }
    }

    return 'node/' . $values->_entity_properties['nid'];
  }
}
