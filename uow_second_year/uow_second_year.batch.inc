<?php

/**
 * @file
 *
 * Batch operations.
 */

/**
 * Update the Course Years field batch processing callback.
 *
 * @param $context
 * @throws \Exception
 */
function uow_second_year_fill_course_years_process(&$context) {
  $ct_to_update = array(UOW_CORE_CT_COURSE, UOW_CORE_CT_COURSE_EDITORIAL);

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress']    = 0;
    $context['sandbox']['current_nid'] = 0;

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_course_years', 'years',
      'n.nid=years.entity_id');
    $query->fields('n', array('nid'))
      ->condition('n.type', $ct_to_update)
      ->isNull('years.field_course_years_tid');
    $total_nodes = $query->countQuery()->execute()->fetchField();

    $context['sandbox']['max'] = $total_nodes;
  }

  $current_year_tid = uow_second_year_get_current_year_tid();

  $limit = 10;
  $query = db_select('node', 'n');
  $query->leftJoin('field_data_field_course_years', 'years',
    'n.nid=years.entity_id');
  $query->fields('n', array('nid'))
    ->condition('n.type', $ct_to_update)
    ->isNull('years.field_course_years_tid')
    ->condition('n.nid', $context['sandbox']['current_nid'], '>')
    ->range(0, $limit)
    ->orderBy('nid', 'ASC');
  $nodes = $query->execute();

  foreach ($nodes as $n) {
    $node                     = node_load($n->nid);
    $node->field_course_years = array(
      LANGUAGE_NONE => array(0 => array('tid' => $current_year_tid))
    );
    node_save($node);

    $context['results'][] = check_plain($node->title);
    $context['sandbox']['progress']++;
    $context['sandbox']['current_nid'] = $n->nid;
    $context['message']                = t('Now processing %node',
      array('%node' => $node->title));
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Update the Course Years field batch finished callback.
 */
function uow_second_year_fill_course_years_finished($success, $results, $operations) {
  if ($success) {
    $message = t('@count items successfully processed:',
      array('@count' => count($results)));

    $message .= theme('item_list', array('items' => $results));
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message         = t('An error occurred while processing %error_operation with arguments: @arguments',
      array(
        '%error_operation' => $error_operation[0],
        '@arguments'       => print_r($error_operation[1], TRUE)
      ));
    drupal_set_message($message, 'error');
  }
}
