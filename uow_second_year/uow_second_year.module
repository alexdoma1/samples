<?php

/**
 * @file
 * Module hooks.
 */

module_load_include('inc', 'uow_second_year');

/**
 * Will be showed course for the current year.
 */
define('UOW_SECOND_YEAR_CURRENT_YEAR', 0);

/**
 * Will be showed course for the next year.
 */
define('UOW_SECOND_YEAR_NEXT_YEAR', 1);

/**
 * Id for current year tab on the courses page.
 */
define('UOW_SECOND_YEAR_TAB_CURRENT_YEAR', 0);

/**
 * Id for next year tab on the courses page
 */
define('UOW_SECOND_YEAR_TAB_NEXT_YEAR', 1);

/**
 * Value indicating second-year course pages and used in URLs.
 */
define('UOW_SECOND_YEAR_NEXT_YEAR_STRING', 'next-year');

/**
 * Value indicating current-year course pages and used in fieldable tabs.
 */
define('UOW_SECOND_YEAR_CURRENT_YEAR_STRING', 'current-year');

/**
 * Will hide button "Apply via UCAS".
 */
define('UOW_SECOND_YEAR_HIDE_BUTTON_APPLY', TRUE);

/**
 * Hide next year tab on courses page.
 */
define('UOW_SECOND_YEAR_HIDE_NEXT_YEAR_TAB', TRUE);

/**
 * Allow user to view/edit hidden second year courses.
 */
define('UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES', 'access hidden second year courses');

/**
 * Undergraduate with Foundation courses tab label.
 */
define('UOW_SECOND_YEAR_UNDGRADUATE_FOUNDATION_LABEL', 'Undergraduate with Foundation courses');

/**
 * Implements hook_entity_view().
 */
function uow_second_year_entity_view($entity, $type, $view_mode, $langcode) {
  if ($type == 'node' && $view_mode == 'full' && $entity->type === UOW_CORE_CT_COURSE_LANDING_PAGE) {
    uow_second_year_prepare_course_data($entity);
  }
}

/**
 * Implements hook_url_inbound_alter().
 */
function uow_second_year_url_inbound_alter(&$path, $original_path, $path_language) {
  if (preg_match('|^node(/.*)\?' . UOW_SECOND_YEAR_NEXT_YEAR_STRING . '(.*)?|',
    $path, $matches)) {
    $_GET[UOW_SECOND_YEAR_NEXT_YEAR_STRING] = UOW_SECOND_YEAR_NEXT_YEAR;
  }
}

/**
 * Implements hook_url_outbound_alter().
 */
function uow_second_year_url_outbound_alter(&$path, &$options, $original_path) {
  if (isset($options['query'][UOW_SECOND_YEAR_NEXT_YEAR_STRING]) && $options['query'][UOW_SECOND_YEAR_NEXT_YEAR_STRING] == 1) {
    $path_params = path_load($path);
    if (!empty($path_params['alias'])) {
      $pattern     = '/(.*)(postgraduate-courses|undergraduate-courses|foundation-courses)(.*)?/';
      $replacement = '$1${2}/' . UOW_SECOND_YEAR_NEXT_YEAR_STRING . '$3';
      if (strpos($path_params['alias'],
          UOW_SECOND_YEAR_NEXT_YEAR_STRING) === FALSE
      ) {
        $path = preg_replace($pattern, $replacement, $path_params['alias']);
      }

      unset($options['query'][UOW_SECOND_YEAR_NEXT_YEAR_STRING]);
      $options['alias'] = TRUE;
    }
  }
}

/**
 * Implements hook_menu().
 */
function uow_second_year_menu() {
  $items = array();

  $items['admin/config/system/westminster/second-year'] = array(
    'title'            => 'Second year settings',
    'description'      => 'Second year settings.',
    'access arguments' => array(UOW_CORE_PERM_ADMIN),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uow_second_year_form_site_settings'),
  );

  $items['admin/config/system/westminster-fees/entry-apply-button'] = array(
    'title'            => 'Direct entry apply button',
    'type'             => MENU_LOCAL_TASK,
    'description'      => 'Direct entry apply button.',
    'access arguments' => array(UOW_CORE_PERM_ADMIN),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('uow_second_year_entry_apply_button_settings'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function uow_second_year_permission() {
  return array(
    UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES => array(
      'title'       => t('Access hidden second year courses'),
      'description' => t('Allow user to access hidden second year courses.'),
    ),
  );
}

/**
 * Settings form for module based fees.
 */
function uow_second_year_form_site_settings() {
  $form = array();

  $defaults                                  = uow_second_year_get_settings();
  $form['uow_second_year_settings']['#tree'] = TRUE;

  $form['uow_second_year_settings']['title'] = array(
    '#markup' => t('<h4>Current year: <b>@year</b></h4>',
      array('@year' => uow_second_year_get_current_year_label())),
  );

  $form['uow_second_year_settings']['next_year'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Next year fallback link title'),
    '#default_value' => $defaults['next_year'],
  );

  $form['uow_second_year_settings']['current_year'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Current year fallback link title.'),
    '#default_value' => $defaults['current_year'],
  );

  $form['uow_second_year_settings']['hide_apply_button_ug'] = array(
    '#title'         => t('Hide button'),
    '#type'          => 'checkbox',
    '#default_value' => $defaults['hide_apply_button_ug'],
    '#description'   => t('Hide the Apply button for UG courses.'),
  );

  $form['uow_second_year_settings']['hide_apply_button_ug_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Display the text when the Apply button for UG courses is hidden.'),
    '#default_value' => $defaults['hide_apply_button_ug_text'],
  );

  $form['uow_second_year_settings']['hide_apply_button_pg'] = array(
    '#title'         => t('Hide button'),
    '#type'          => 'checkbox',
    '#default_value' => $defaults['hide_apply_button_pg'],
    '#description'   => t('Hide the Apply button for PG courses.'),
  );

  $form['uow_second_year_settings']['hide_apply_button_pg_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Display the text when the Apply button for PG courses is hidden.'),
    '#default_value' => $defaults['hide_apply_button_pg_text'],
  );

  $form['uow_second_year_settings']['default_accordion_tab'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Entity id of fieldable accordion pane.'),
    '#default_value' => $defaults['default_accordion_tab'],
    '#description'   => t('Need for second year functionality. Link to entities list - admin/structure/fieldable-panels-panes/generic_content_page_accordion/list'),
  );

  $form['uow_second_year_settings']['hide_courses'] = array(
    '#title'         => t('Hide second year courses'),
    '#type'          => 'checkboxes',
    '#default_value' => $defaults['hide_courses'],
    '#description'   => t('Hide second years tabs in front office. Don\'t display tabs and navigation by course year'),
    '#options'       => uow_second_year_course_level_options(),
  );

  $form['update_course_years'] = array(
    '#type'   => 'submit',
    '#value'  => t('Update the Course years field'),
    '#suffix' =>
      '<div class="description">' .
      t('This operation fills the Course Years field for <b>all</b> Course Import and Course Editorial ' .
        'nodes that have this field empty. ' .
        'The value is set to current admission year (@year). IMPORTANT! This update should be executed before the' .
        'first content import.',
        array('@year' => uow_second_year_get_current_year_label())) .
      '</div>',
    '#submit' => array('uow_second_year_fill_course_years_submit'),
  );

  $form['#submit'][] = 'uow_second_year_form_site_settings_submit';

  return system_settings_form($form);
}

/**
 * Second Year settings form submission callback. Creates batch that updates
 * the Course Years field.
 *
 * @param $form
 * @param $form_state
 */
function uow_second_year_fill_course_years_submit($form, &$form_state) {
  // Create course years terms.
  uow_second_year_create_course_years();

  // Create a batch to fill the Course Years field.
  $batch = array(
    'operations'       => array(
      array('uow_second_year_fill_course_years_process', array()),
    ),
    'finished'         => 'uow_second_year_fill_course_years_finished',
    'title'            => t('Updating Course Years field'),
    'init_message'     => t('Starting the update.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('The process has encountered an error.'),
    'file'             => drupal_get_path('module',
        'uow_second_year') . '/uow_second_year.batch.inc',
  );

  // Set batch. It will run automatically.
  batch_set($batch);
}

/**
 * Settings form for entry apply button.
 */
function uow_second_year_entry_apply_button_settings() {
  $defaults = uow_second_year_get_entry_apply_button_settings();
  $form     = array();

  $form['entry_apply_button_settings']['#tree'] = TRUE;

  $form['entry_apply_button_settings']['hide_parttime_apply_button_current_year']      = array(
    '#title'         => t('Hide button'),
    '#type'          => 'checkbox',
    '#default_value' => $defaults['hide_parttime_apply_button_current_year'],
    '#description'   => t('Hide the "Apply for this part-time course now" button (current year)'),
  );
  $form['entry_apply_button_settings']['hide_parttime_apply_button_current_year_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Display the text when the Apply button for UG part-time courses is hidden (current year).'),
    '#default_value' => $defaults['hide_parttime_apply_button_current_year_text'],
  );
  $form['entry_apply_button_settings']['hide_parttime_apply_button_next_year']         = array(
    '#title'         => t('Hide button'),
    '#type'          => 'checkbox',
    '#default_value' => $defaults['hide_parttime_apply_button_next_year'],
    '#description'   => t('Hide the "Apply for this part-time course now" button (next year)'),
  );
  $form['entry_apply_button_settings']['hide_parttime_apply_button_next_year_text']    = array(
    '#type'          => 'textarea',
    '#title'         => t('Display the text when the Apply button for UG part-time courses is hidden (next year).'),
    '#default_value' => $defaults['hide_parttime_apply_button_next_year_text'],
  );

  $form['#submit'][] = 'uow_second_year_entry_apply_button_settings_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for uow_second_year_form_site_settings form.
 */
function uow_second_year_entry_apply_button_settings_submit(&$form, &$form_state) {
  cache_clear_all('*', 'cache_page', TRUE);

  uow_second_year_purge_subject_pages();
}

/**
 * Get an associative array tid=>name of course level.
 *
 * @return array
 */
function uow_second_year_course_level_options() {
  return array(
    UOW_IMPORT_LEVEL_POSTGRADUATE_TID => UOW_CORE_TERM_COURSE_LEVEL_POSTGRADUATE,
    UOW_IMPORT_LEVEL_UDEGRATUATE_TID  => UOW_CORE_TERM_COURSE_LEVEL_UNDERGRADUATE,
  );
}

/**
 * Submit handler for uow_second_year_form_site_settings form.
 */
function uow_second_year_form_site_settings_submit(&$form, &$form_state) {
  cache_clear_all('*', 'cache_page', TRUE);

  uow_second_year_purge_subject_pages();
}

/**
 * Get string representation of current academic year.
 */
function uow_second_year_get_current_year_label() {
  $year = &drupal_static(__FUNCTION__);

  if (!isset($year)) {
    $year = variable_get('uow_import_current_admission_year', array(
      'year'      => '2016/17',
      'year_sits' => '2016/7',
      'tid'       => NULL,
    ));
    $year = $year['year'];
  }

  return $year;
}

/**
 * Get string representation of second academic year.
 */
function uow_second_year_get_second_year_label() {
  $year = &drupal_static(__FUNCTION__);

  if (!isset($year)) {
    $year = variable_get('uow_import_current_admission_year', array(
      'year'      => '2016/17',
      'year_sits' => '2016/7',
      'tid'       => NULL,
    ));
    $year = substr($year['year'], 2, 2);
    $year = $year ? '20' . (intval($year) + 1) . '/' . (intval($year) + 2) : '';
  }

  return $year;
}

/**
 * Get taxonomy term ID of current year term.
 */
function uow_second_year_get_current_year_tid() {
  $tid = &drupal_static(__FUNCTION__);

  if (!isset($tid)) {
    $year = variable_get('uow_import_current_admission_year', array(
      'year'      => '2016/17',
      'year_sits' => '2016/7',
      'tid'       => NULL,
    ));

    if (!empty($year['tid'])) {
      $tid = $year['tid'];
    }
    else {
      $term = taxonomy_get_term_by_name($year['year'],
        UOW_IMPORT_VOC_COURSE_YEARS);
      if (!empty($term)) {
        $term = reset($term);
        $tid  = $term->tid;
      }
      else {
        $vocabulary = taxonomy_vocabulary_machine_name_load(UOW_IMPORT_VOC_COURSE_YEARS);
        $term       = (object) array(
          'name'   => $year['year'],
          'vid'    => $vocabulary->vid,
          'weight' => 0,
        );

        $term = taxonomy_term_save($term);
        $tid  = $term->tid;
      }

      variable_set('uow_import_current_admission_year', array(
        'year'      => $term->name,
        'year_sits' => '2016/7',
        'tid'       => $tid,
      ));
    }
  }

  return $tid;
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function uow_second_year_ctools_plugin_directory($module, $plugin) {
  if ($module == 'panels' && $plugin == 'styles') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_views_query_alter().
 */
function uow_second_year_views_query_alter(&$view, &$query) {
  if ($view->name == 'uow_list_undergraduate_subjects' || $view->name == 'uow_list_postgraduate_subjects') {
    if ($view->current_display == 'ug_courses_current_year' || $view->current_display == 'pg_courses_current_year') {
      $tid = variable_get('uow_import_current_admission_year');
      $tid = $tid['tid'];

      $query->where[1]['conditions'][3]['value'] = $tid;
    }
    elseif ($view->current_display == 'ug_courses_next_year' || $view->current_display == 'pg_courses_next_year') {
      $next_year = uow_second_year_get_second_year_label();
      $term      = taxonomy_get_term_by_name($next_year,
        UOW_IMPORT_VOC_COURSE_YEARS);

      if (!empty($term)) {
        $term = array_shift($term);
        $tid  = $term->tid;

        $query->where[1]['conditions'][3]['value'] = $tid;
      }
    }
  }
  elseif ($view->name == 'uow_list_current_year_courses_by_subject_on_subject_landing') {
    $tid = variable_get('uow_import_current_admission_year');
    $tid = $tid['tid'];

    $query->where[1]['conditions'][2]['value'] = $tid;
  }
  elseif ($view->name == 'uow_list_next_year_courses_by_subject_on_subject_landing') {
    $next_year = uow_second_year_get_second_year_label();
    $term      = taxonomy_get_term_by_name($next_year,
      UOW_IMPORT_VOC_COURSE_YEARS);

    if (!empty($term)) {
      $term = array_shift($term);
      $tid  = $term->tid;

      $query->where[1]['conditions'][2]['value'] = $tid;
    }
  }
}

/**
 * Implements hook_entity_load().
 */
function uow_second_year_entity_load($entities, $type) {
  if ($type == 'fieldable_panels_pane' && arg(0) == 'taxonomy' && arg(1) == 'term') {

    foreach ($entities as $key => &$value) {

      if ($value->bundle == 'generic_content_page_accordion') {

        if (!isset($value->field_accordion_tab[LANGUAGE_NONE])) {
          $value->field_accordion_tab                = array();
          $value->field_accordion_tab[LANGUAGE_NONE] = array();
        }
        $tabs_array = $value->field_accordion_tab[LANGUAGE_NONE];

        $additional_tabs = uow_second_year_get_accordion_additional_tabs();
        if ($additional_tabs) {

          foreach ($additional_tabs as $key => $tab) {
            $additional_tab = array(
              'value'       => $tab->field_accordion_tab_value,
              'revision_id' => $tab->field_accordion_tab_revision_id,
            );

            array_unshift($tabs_array, $additional_tab);
          }

        }

        $value->field_accordion_tab[LANGUAGE_NONE] = $tabs_array;

      }
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function uow_second_year_form_node_form_alter(&$form, &$form_state, $form_id) {
  // Disable all fields that shouldn't be edited manually. The content of
  // these fields is copied from 'current year' course editorial.
  $is_course_editorial_edit_form = ($form['#bundle'] === UOW_CORE_CT_COURSE_EDITORIAL)
    && !empty($form['nid']['#value']);

  if (!$is_course_editorial_edit_form) {
    return;
  }

  $node = $form['#node'];

  $form['field_course_years']['#access'] = FALSE;

  $current_year    = variable_get('uow_import_current_admission_year');
  $course_year_tid = uow_core_field_get_item('node', $node,
    'field_course_years', 0, 'tid');

  // If current editorial is Second Year editorial.
  if ($course_year_tid != $current_year['tid']) {
    $editable_fields = array(
      'field_course_overview',
      'field_course_overview_paragraph',
      'field_entry_requirements',
      'field_add_kis_widget',
    );

    foreach ($form as $key => $field) {
      if (mb_strpos($key, 'field_') === 0 && !in_array($key,
          $editable_fields)
      ) {
        $form[$key]['#access'] = FALSE;
      }
    }

    $form['field_add_kis_widget'][LANGUAGE_NONE]['#access'] = FALSE;
  }

  // Add year navigation dropdown.
  // Get related landing page.
  $result  = views_get_view_result(UOW_CORE_VIEW_QUERY_COURSE_LANDING_PAGE_BY_COURSE_EDITORIAL,
    'default', $node->nid);
  $result  = reset($result);
  $landing = node_load($result->nid);

  // Get editorials, attached to the landing page.
  if (!empty($landing)) {
    $editorials = uow_core_field_get_item('node', $landing,
      'field_course_content');

    $editorials = array_map(function ($item) {
      return $item['target_id'];
    }, $editorials);
  }

  $editorials = node_load_multiple($editorials);
  if (count($editorials) > 1) {
    foreach ($editorials as $editorial) {
      $editorial_year      = uow_core_field_get_item('node', $editorial,
        'field_course_years', 0, 'tid');
      $editorial_year_term = taxonomy_term_load($editorial_year);
      if (!empty($editorial_year_term)) {
        $year_options[$editorial->nid] = $editorial_year_term->name;
      }
    }

    if (count($year_options) > 1) {
      $form['year_navigation'] = array(
        '#type'          => 'select',
        '#options'       => $year_options,
        '#weight'        => -1,
        '#title'         => t('Select attendance year to edit'),
        '#default_value' => $node->nid,
        '#ajax'          => array(
          'callback' => 'uow_second_year_year_navigation_redirect',
        ),
      );
    }
  }
}

/**
 * Redirect user to node edit page of current/second attendance year course
 * editorial.
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function uow_second_year_year_navigation_redirect($form, &$form_state) {
  ctools_include('ajax');

  $commands = array();

  $commands[] = ctools_ajax_command_redirect('node/' . $form_state['values']['year_navigation'] . '/edit');
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Implements hook_node_update().
 */
function uow_second_year_node_update($node) {
  // Find related second year course editorial nodes and copy content to them.
  $current_year    = variable_get('uow_import_current_admission_year');
  $course_year_tid = uow_core_field_get_item('node', $node,
    'field_course_years', 0, 'tid');

  $is_current_year_editorial = ($node->type === UOW_CORE_CT_COURSE_EDITORIAL)
    && empty($node->is_second_year_course)
    && ($course_year_tid == $current_year['tid']);

  if ($is_current_year_editorial) {
    $result = views_get_view_result(UOW_CORE_VIEW_QUERY_COURSE_LANDING_PAGE_BY_COURSE_EDITORIAL,
      'default', $node->nid);

    foreach ($result as $landing) {
      $landing_node = node_load($landing->nid);
      if (empty($landing_node)) {
        continue;
      }

      $second_year_editorial_nid = uow_core_field_get_item('node',
        $landing_node, 'field_course_content',
        UOW_IMPORT_INDEX_COURSE_NEXT_YEAR, 'target_id');

      $second_year_editorial = node_load($second_year_editorial_nid);

      if (!empty($second_year_editorial)) {
        uow_second_year_copy_course_editorial_fields($node,
          $second_year_editorial);
        // Set 'Is second year course' flag to prevent cloning of this node
        // in hook_node_update().
        $second_year_editorial->is_second_year_course = TRUE;
        node_save($second_year_editorial);
      }
    }
  }
}

/**
 * Copy field values from current year course editorial to second year editorial
 * node excluding Entry requirements and Course overview.
 *
 * @param $source
 * @param $destination
 */
function uow_second_year_copy_course_editorial_fields($source, $destination) {
  $source_wrapper = entity_metadata_wrapper('node', $source);
  $dest_wrapper   = entity_metadata_wrapper('node', $destination);
  $fields         = array_keys(get_object_vars($source));

  $clone_all = variable_get('uow_second_year_clone_all', FALSE);

  // If destination node is new, then we clone all fields except course years.
  if (!empty($destination->is_new) || $clone_all) {
    $exclude_fields = array(
      'field_course_years',
    );
  }
  else {
    // These fields are editable and shouldn't be copied.
    $exclude_fields = array(
      'field_course_years',
      'field_course_overview',
      'field_course_overview_paragraph',
      'field_entry_requirements',
    );
  }

  // Perform a deep copy of node fields.
  foreach ($fields as $field_name) {
    if ((mb_strpos($field_name, 'field_') === 0) && !in_array($field_name,
        $exclude_fields)
    ) {
      $field_info = field_info_field($field_name);
      if ($field_info['type'] === 'paragraphs') {
        // Delete old paragraphs from destination node.
        $dest_paragraphs = $dest_wrapper->{$field_name}->value();

        if (!empty($dest_paragraphs) && !is_array($dest_paragraphs)) {
          $dest_paragraphs = array($dest_paragraphs);
        }

        foreach ($dest_paragraphs as $old_paragraph) {
          if (!empty($old_paragraph)) { // Paragraph can be NULL.
            $old_paragraph->delete();
          }
        }

        unset($destination->{$field_name});

        // Clone paragraphs from source to destination.
        $source_paragraphs = $source_wrapper->{$field_name}->value();

        if (!is_array($source_paragraphs)) {
          $source_paragraphs = array($source_paragraphs);
        }
        foreach ($source_paragraphs as $index => $paragraph) {
          if (!empty($paragraph)) { // Sometimes paragraph is NULL.
            list(, , $bundle) = entity_extract_ids('paragraphs_item',
              $paragraph);

            $new_item = entity_create('paragraphs_item',
              array('bundle' => $bundle, 'field_name' => $field_name));
            $new_item->setHostEntity('node', $destination);

            foreach (field_info_instances('paragraphs_item',
              $bundle) as $new_field_name => $new_field_instance) {
              if (!empty($paragraph->{$new_field_name})) {
                uow_second_year_clone_field($paragraph, $new_item,
                  $new_field_name);
              }
            }
          }
        }
      }
      else {
        uow_second_year_clone_field($source, $destination, $field_name);
      }
    }
  }

  // Clone other fields.
  $destination->title  = $source->title;
  $destination->status = $source->status;
}

/**
 * Clone simple fields from one entity to another. Does not create new entities
 * such as paragraphs or terms, just clones references.
 *
 * @param $source
 * @param $destination
 * @param $field_name
 */
function uow_second_year_clone_field($source, $destination, $field_name) {
  foreach ($source->{$field_name} as $lang => $lang_data) {
    foreach ($source->{$field_name}[$lang] as $item_id => $item) {
      foreach ($source->{$field_name}[$lang][$item_id] as $item_key => $item_value) {
        $destination->{$field_name}[$lang][$item_id][$item_key] = $item_value;
      }
    }
  }
}

/**
 * Create course year terms if the vocabulary is empty.
 */
function uow_second_year_create_course_years() {
  $vocabulary = taxonomy_vocabulary_machine_name_load(UOW_IMPORT_VOC_COURSE_YEARS);
  $tree       = taxonomy_get_tree($vocabulary->vid);

  if (!empty($tree)) {
    return;
  }

  // Create terms for attendance years.
  $start_first_year = 2016;
  $end_first_year   = 17;

  $start_last_year = 2098;
  $end_last_year   = 99;

  $years_range = array_combine(range($start_first_year, $start_last_year),
    range($end_first_year, $end_last_year));

  $weight = 0;
  foreach ($years_range as $key => $value) {
    $term = (object) array(
      'name'   => $key . '/' . $value,
      'vid'    => $vocabulary->vid,
      'weight' => $weight,
    );

    taxonomy_term_save($term);

    $weight++;
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function uow_second_year_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  foreach ($data['tabs'] as $key1 => $tabs) {
    if (isset($tabs['output'])) {
      foreach ($tabs['output'] as $key2 => $item) {
        // Conditionally change link title.
        if ($item['#link']['path'] == 'node/%/editorial/%') {
          $year_param = uow_second_year_check_is_nextyear();
          $data['tabs'][$key1]['output'][$key2]['#link']['href'] .= $year_param;
        }
      }
    }
  }
}

/**
 * Implements hook_init().
 */
function uow_second_year_init() {
  $node = menu_get_object();

  $is_landing_node = !empty($node) && ($node->type === UOW_CORE_CT_COURSE_LANDING_PAGE);
  if (!$is_landing_node) {
    return;
  }

  // If we are on a "Course Landing Page" node type and we arrive without a subject, redirect to the node
  // with the subject as well. We choose the first delta of the subject in case of multiples.
  if (empty(arg(2))) {
    $current_year_nid = uow_core_field_get_item('node', $node, 'field_course_data', UOW_IMPORT_INDEX_COURSE_CURRENT_YEAR, 'target_id');
    $next_year_nid = uow_core_field_get_item('node', $node, 'field_course_data', UOW_IMPORT_INDEX_COURSE_NEXT_YEAR, 'target_id');

    // If current year course data is not published, then redirect to next-year
    // course landing page.
    $options = array();
    $current_year_data = node_load($current_year_nid);
    if (!empty($current_year_data) && $current_year_data->status == NODE_NOT_PUBLISHED) {
      $next_year_data = node_load($next_year_nid);

      if (!empty($next_year_data) && $next_year_data == NODE_PUBLISHED) {
        $options['query'] = array(UOW_SECOND_YEAR_NEXT_YEAR_STRING => 1);
      }
    }

    $url_parts  = array('node', $node->nid);
    $subject_id = uow_core_field_get_item('node', $node, 'field_subject', 0,
      'target_id');
    if (taxonomy_term_load($subject_id)) {
      $url_parts[] = $subject_id;
      drupal_goto(implode('/', $url_parts), $options, 301);
    }
  }
  elseif (is_numeric(arg(2))) {
    $index = uow_second_year_check_is_nextyear() ? UOW_IMPORT_INDEX_COURSE_NEXT_YEAR : UOW_IMPORT_INDEX_COURSE_CURRENT_YEAR;

    $course_data_nid = uow_core_field_get_item('node', $node,
      'field_course_data', $index, 'target_id');
    $course_data     = node_load($course_data_nid);

    if (!empty($course_data)) {
      $course_level = uow_core_field_get_item('node', $course_data,
        'field_course_level', 0, 'tid');

      $is_not_published_or_hidden_next_year = ($course_data->status == NODE_NOT_PUBLISHED)
        || ($index === UOW_IMPORT_INDEX_COURSE_NEXT_YEAR) && uow_second_year_is_next_year_hidden_for_course_level($course_level);
      if ($is_not_published_or_hidden_next_year) {
        drupal_not_found();
      }
    }
  }
}

/**
 * Implements hook_search_api_query_alter().
 *
 * @param \SearchApiQueryInterface $query
 */
function uow_second_year_search_api_query_alter(SearchApiQueryInterface $query) {
  $index = $query->getIndex();

  if (!in_array($index->machine_name, array('courses', 'site_search'))) {
    return;
  }

  // Hide next year UG or PG courses if needed.
  $current_year = variable_get('uow_import_current_admission_year');

  // Load year settings and check permissions.
  $settings               = uow_second_year_get_settings();
  $is_next_year_ug_hidden = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES) && !empty($settings['hide_courses'][UOW_IMPORT_LEVEL_UDEGRATUATE_TID]);
  $is_next_year_pg_hidden = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES) && !empty($settings['hide_courses'][UOW_IMPORT_LEVEL_POSTGRADUATE_TID]);

  $base_filter = $query->createFilter('OR');
  // To match any content type.
  $base_filter->condition('field_course_level', NULL);

  // To filter UG, FC, FD courses by year, as they're using the same setting.
  $course_levels = array(
    UOW_IMPORT_LEVEL_FOUNDATION_COURSE_TID,
    UOW_IMPORT_LEVEL_UDEGRATUATE_WITH_FOUNDATION_TID,
    UOW_IMPORT_LEVEL_UDEGRATUATE_TID,
  );

  foreach ($course_levels as $course_level) {
    $level_filter = $query->createFilter('AND');
    $level_filter->condition('field_course_level', $course_level);

    if ($is_next_year_ug_hidden) {
      $level_filter->condition('field_course_years', $current_year['tid']);
    }

    $base_filter->filter($level_filter);
  }

  // To filter PG courses by year.
  $pg_filter = $query->createFilter('AND');
  $pg_filter->condition('field_course_level',
    UOW_IMPORT_LEVEL_POSTGRADUATE_TID);

  if ($is_next_year_pg_hidden) {
    $pg_filter->condition('field_course_years', $current_year['tid']);
  }

  $base_filter->filter($pg_filter);

  $query->filter($base_filter);
}

/**
 * Check if next year of specific course level should be hidden.
 */
function uow_second_year_is_next_year_hidden_for_course_level($course_level) {
  $settings               = uow_second_year_get_settings();
  $is_next_year_ug_hidden = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES) && !empty($settings['hide_courses'][UOW_IMPORT_LEVEL_UDEGRATUATE_TID]);
  $is_next_year_pg_hidden = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES) && !empty($settings['hide_courses'][UOW_IMPORT_LEVEL_POSTGRADUATE_TID]);

  $course_levels = array(
    UOW_IMPORT_LEVEL_FOUNDATION_COURSE_TID,
    UOW_IMPORT_LEVEL_UDEGRATUATE_WITH_FOUNDATION_TID,
    UOW_IMPORT_LEVEL_UDEGRATUATE_TID,
  );

  if (in_array($course_level, $course_levels)) {
    return $is_next_year_ug_hidden;
  }

  if (UOW_IMPORT_LEVEL_POSTGRADUATE_TID == $course_level) {
    return $is_next_year_pg_hidden;
  }

  return FALSE;
}

/**
 * Implements hook_pathauto().
 */
function uow_second_year_pathauto($op) {
  switch ($op) {
    case 'settings':
      $settings                          = array();
      $settings['module']                = 'uow_second_year';
      $settings['groupheader']           = t('UoW Second year alias');
      $settings['patterndescr']          = t('All setting from this field will be ignored during alias creation process.');
      $settings['patterndefault']        = '';
      $settings['batch_update_callback'] = 'uow_second_year_bulk_update_batch_process';
      $settings['batch_file']            = drupal_get_path('module',
          'uow_second_year') . '/uow_second_year.inc';
      return (object) $settings;
    default:
      break;
  }
}

/**
 * Implements hook_views_data().
 */
function uow_second_year_views_data() {
  $data['uow_second_year']['table']['group'] = t('UOW Second Year');
  $data['uow_second_year']['table']['join']  = array(
    // Exist in all views.
    '#global' => array(),
  );

  $data['uow_second_year']['second_year_url_field'] = array(
    'title' => t('Second year field Url'),
    'help'  => t('Adds the item\'s URL to the node or landing node.'),
    'field' => array(
      'handler' => 'views_handler_second_year_url_field',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_api().
 */
function uow_second_year_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_entity_property_info_alter().
 */
function uow_second_year_entity_property_info_alter(&$info) {
  $info['node']['bundles'][UOW_CORE_CT_COURSE]['properties']['related_course_editorial'] = array(
    'type'            => 'node',
    'label'           => t('Related course editorial'),
    'getter callback' => 'uow_second_year_related_course_editorial_getter_callback',
  );
}

/**
 * Related course editorial getter callback.
 *
 * @param $item
 */
function uow_second_year_related_course_editorial_getter_callback($node) {
  // Add a property containing appropriate Course Editorial node
  // to Course Import node.
  $field_course_code = uow_core_field_get_item('node', $node,
    'field_course_code', 0, 'value');

  $results = views_get_view_result(UOW_IMPORT_QUERY_COURSE_LANDING_PAGE_BY_COURSE_COURSE_CODE,
    'default', $field_course_code);

  if (!empty($results)) {
    $landing_page      = array_shift($results);
    $landing_page_node = node_load($landing_page->nid);

    foreach ($landing_page_node->field_course_data[LANGUAGE_NONE] as $delta => $field_course_data) {
      if ($field_course_data['target_id'] == $node->nid) {
        $editorial_nid = uow_core_field_get_item('node', $landing_page_node,
          'field_course_content', $delta, 'target_id');

        return node_load($editorial_nid);
      }
    }
  }

  return FALSE;
}
