<?php

/**
 * @file
 * Definition of the 'Tabs' panel style.
 */

// Plugin definition.
$plugin = array(
  'title'         => t('Tabs'),
  'description'   => t('Show panel panes in a region as tabs.'),
  'render region' => 'uow_second_year_style_render_region',
  'settings form' => 'uow_second_year_pane_settings_form',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_uow_second_year_style_render_region($vars) {
  $display   = $vars['display'];
  $region_id = $vars['region_id'];
  $owner_id  = $vars['owner_id'];
  $panes     = $vars['panes'];
  $settings  = $vars['settings'];

  $active_tab_position = UOW_SECOND_YEAR_TAB_CURRENT_YEAR;
  if (!empty($_GET[UOW_SECOND_YEAR_NEXT_YEAR_STRING])) {
    $active_tab_position = UOW_SECOND_YEAR_TAB_NEXT_YEAR;
  }

  $defaults_settings = uow_second_year_get_settings();
  $hide_next_year    = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES)
    && !empty($defaults_settings['hide_courses'][$settings['course_level']]);

  $tab_id = 'tabs-' . $owner_id . '-' . $region_id;

  $region_title = '';
  if (!empty($settings['region_title'])) {
    $context      = isset($vars['renderer']->display_context) ? $vars['renderer']->display_context : $display->context;
    $region_title = check_plain(ctools_context_keyword_substitute($settings['region_title'],
      array(), $context));
  }

  $element = array(
    '#prefix' => '<p><strong>' . $region_title . '</strong></p>' . '<div class="second-year-tabs-block" id="' . $tab_id . '">',
    '#suffix' => '</div>',
  );

  // Get the pane titles.
  $items       = array();
  $items_no_js = array();
  $delta       = 0;

  if (isset($display->panels[$region_id])) {
    foreach ($display->panels[$region_id] as $pane_id) {
      // Make sure the pane exists.
      if (!empty($panes[$pane_id])) {

        $active_class = '';
        if ($active_tab_position == $delta) {
          $active_class = 'active';
        }
        else {
          // This text will appear when pane is empty.
          $placeholder_text = 'Placeholder for empty or inaccessible';
          if (strpos($panes[$pane_id], $placeholder_text) !== FALSE) {
            $hide_next_year = TRUE;
          }
        }

        $no_js_url = str_replace('/next-year', '', request_path());
        if (!$delta) {
          $title = uow_second_year_get_current_year_label();

        }
        else {
          $title = uow_second_year_get_second_year_label();
          $no_js_url .= '/next-year';
        }
        $title .= ' entry';

        $item = array(
          'data'  => '<a data-toggle="tab" href="#' . $tab_id . '-' . $delta . '">' . $title . '</a>',
          'class' => array($active_class),
        );

        $item_no_js = array(
          'data'  => '<a data-toggle="tab" href="' . '/' . $no_js_url . '">' . $title . '</a>',
          'class' => array($active_class),
        );

        $items[]       = $item;
        $items_no_js[] = $item_no_js;
        $delta++;
      }
    }
  }

  if ($delta === 0) {
    // No tabs to show, the tabs wrapper must not be rendered.
    return '';
  }

  if (!$hide_next_year) {
    $element['tabs_title']       = array(
      '#theme'      => 'item_list',
      '#items'      => $items,
      '#attributes' => array(
        'class' => array('nav required-fields group-tabs nav-tabs second-year-tabs-header'),
      ),
      '#prefix'     => '<div id="second-year-tabs-with-js" style="display:none;">',
      '#suffix'     => '</div>',
    );
    $element['tabs_title_no_js'] = array(
      '#theme'      => 'item_list',
      '#items'      => $items_no_js,
      '#attributes' => array(
        'class' => array('nav required-fields group-tabs nav-tabs second-year-tabs-header'),
      ),
      '#prefix'     => '<noscript>',
      '#suffix'     => '</noscript>',
    );
  }

  $element['tabs_content_before'] = array(
    '#markup' => '<div class="tab-content required-fields group-tabs">',
  );

  $delta = 0;

  foreach ($panes as $pane_id => $item) {
    $active_class = '';
    if ($active_tab_position == $delta) {
      $active_class = 'active';
    }
    elseif ($hide_next_year) {
      continue;
    }

    $element['tabs_content'][$pane_id] = array(
      '#prefix' => '<div class="tab-pane ' . $active_class . '" id="' . $tab_id . '-' . $delta . '">',
      '#suffix' => '</div>',
      '#markup' => $item,
    );
    $delta++;
  }

  $element['tabs_content_after'] = array(
    '#markup' => '</div>',
  );

  return drupal_render($element);
}

/**
 * Settings form for the plugin.
 */
function uow_second_year_pane_settings_form($style_settings) {
  $form = array();

  $form['region_title'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Title'),
    '#description'   => t('Optional title of the region.'),
    '#required'      => FALSE,
    '#default_value' => (isset($style_settings['region_title'])) ? $style_settings['region_title'] : '',
  );

  $form['course_level'] = array(
    '#type'          => 'select',
    '#title'         => t('Select course level of visible tabs.'),
    '#options'       => uow_second_year_course_level_options(),
    '#default_value' => isset($style_settings['course_level']) ? $style_settings['course_level'] : UOW_IMPORT_LEVEL_UDEGRATUATE_TID,
  );

  return $form;
}
