<?php
/**
 * @file
 * Helper functions.
 */

/**
 * The list of options for the course state and its default messages.
 */
function uow_second_year_prepare_course_data($entity) {
  $year_param                                        = uow_second_year_check_is_nextyear();
  $entity->content['field_course_data']['#items']    = array_slice($entity->content['field_course_data']['#items'],
    $year_param, 1, TRUE);
  $entity->content['field_course_content']['#items'] = array_slice($entity->content['field_course_content']['#items'],
    $year_param, 1, TRUE);
}

/**
 * Get current course for landing page, depends of nextyear.
 */
function uow_second_year_get_course($node) {
  $year_param = uow_second_year_check_is_nextyear();
  $course     = uow_core_field_get_item('node', $node, 'field_course_data',
    $year_param, 'target_id');

  return $course;
}

/**
 * Check is this next year.
 */
function uow_second_year_check_is_nextyear() {
  $query_params = drupal_get_query_parameters();
  $year_param   = UOW_SECOND_YEAR_CURRENT_YEAR;
  if (isset($query_params[UOW_SECOND_YEAR_NEXT_YEAR_STRING]) && $query_params[UOW_SECOND_YEAR_NEXT_YEAR_STRING] == UOW_SECOND_YEAR_NEXT_YEAR) {
    $year_param = UOW_SECOND_YEAR_NEXT_YEAR;
  }

  return $year_param;
}

/**
 * Helper function.
 *
 * Replace widget token with dropdown html.
 */
function uow_second_year_replace_years_dropdown($text, $node) {
  $dropdown_years  = array();
  $courses_content = uow_core_field_get_item('node', $node,
    'field_course_content');

  $courses_data = uow_core_field_get_item('node', $node,
    'field_course_data');

  if ($subject = arg(2)) {
    $subject = str_replace('?next-year=1', '', $subject);
    if (taxonomy_term_load($subject)) {
      $default_subject = $subject;
    }
  }
  else {
    $default_subject = uow_core_field_get_item('node', $node, 'field_subject',
      0, 'target_id');
  }

  $defaults = uow_second_year_get_settings();

  $courses_data_nodes = array();
  foreach ($courses_data as $key => $item) {
    if (!empty($item['entity'])) {
      $courses_data_nodes[] = $item['entity'];
    }
    else {
      $courses_data_nodes[] = node_load($item['target_id']);
    }

    if ($courses_data_nodes[$key]->status == NODE_NOT_PUBLISHED) {
      unset($courses_content[$key]);
    }
  }

  $course_level = uow_core_field_get_item('node', $courses_data_nodes[0],
    'field_course_level', 0, 'tid');

  $hide_next_year_dropdown = uow_second_year_is_next_year_hidden_for_course_level($course_level);

  $tokens = array(
    '[current-year-url]',
    '[next-year-url]',
  );

  $tokens_value = array();
  if (count($courses_content) > 1 && !$hide_next_year_dropdown) {
    foreach ($courses_content as $key => $course) {
      $tid = uow_core_field_get_item('node', $course['entity'],
        'field_course_years', 0, 'tid');
      if ($tid) {
        $term                        = taxonomy_term_load($tid);
        $dropdown_years[$term->name] = array(
          'path'  => 'node/' . $node->nid . '/' . $default_subject,
          'alias' => '',
        );
      }
    }
    ksort($dropdown_years);
    $last_key = end(array_keys($dropdown_years));
    $dropdown_years[$last_key]['path'] .= '?next-year=1';

    foreach ($dropdown_years as $key => $value) {
      $path_params = path_load($value['path']);
      if (isset($path_params['alias'])) {
        $dropdown_years[$key]['alias'] = $path_params['alias'];
        $tokens_value[]                = '/' . $path_params['alias'];
      }
    }

    // Replace fallback link token.
    $fallback_link_text = $defaults['current_year'];
    if (uow_second_year_check_is_nextyear()) {
      $fallback_link_text = $defaults['next_year'];
    }
    $fallback_link_text = str_replace($tokens, $tokens_value,
      $fallback_link_text);

    drupal_add_js(array('yearsParams' => $dropdown_years), 'setting');
  }

  $widget = str_replace("[TWO_YEARS_FALLBACK_LINK]", $fallback_link_text,
    $text);

  return $widget;
}

/**
 * Helper function.
 *
 * Return fallback link settings.
 */
function uow_second_year_get_settings() {
  return variable_get('uow_second_year_settings', array(
    'next_year'                                    => 'For the next year entry course <strong><a href="[next-year-url]">visit 2017/18</a></strong> page.',
    'current_year'                                 => 'For the current year entry course <strong><a href="[current-year-url]">visit 2016/17</a></strong> page.',
    'hide_apply_button_ug'                         => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_apply_button_pg'                         => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_apply_button_ug_text'                    => '',
    'hide_apply_button_pg_text'                    => '',
    'default_accordion_tab'                        => 39753,
    'hide_courses'                                 => array(
      UOW_IMPORT_LEVEL_POSTGRADUATE_TID => 0,
      UOW_IMPORT_LEVEL_UDEGRATUATE_TID  => 0,
    ),
    'hide_parttime_apply_button_current_year'      => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_parttime_apply_button_next_year'         => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_parttime_apply_button_current_year_text' => '',
    'hide_parttime_apply_button_next_year_text'    => '',
  ));
}

/**
 * Helper function.
 *
 * Return settings form for entry apply button.
 */
function uow_second_year_get_entry_apply_button_settings() {
  return variable_get('entry_apply_button_settings', array(
    'hide_parttime_apply_button_current_year'      => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_parttime_apply_button_next_year'         => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_parttime_apply_button_current_year_text' => '',
    'hide_parttime_apply_button_next_year_text'    => '',
  ));
}

/**
 * Helper function.
 *
 * Replace token on the accordion tabs.
 */
function uow_second_year_prepare_accordion_tabs($text) {
  $tokens_value = array(
    uow_second_year_get_current_year_label(),
    uow_second_year_get_second_year_label(),
  );
  $tokens       = array(
    '[current-year]',
    '[next-year]',
  );

  $text = str_replace($tokens, $tokens_value, $text);

  return $text;
}

/**
 * Clear subjects related pages after saving second year settings.
 */
function uow_second_year_purge_subject_pages() {
  $subjects_vid = variable_get('uow_import_subjects_voc', '21');
  $subjects     = taxonomy_get_tree($subjects_vid);
  foreach ($subjects as $subject) {
    // Add clear path for subject page.
    $paths[] = url('taxonomy/term/' . $subject->tid);
  }

  $paths[] = url('courses/undergraduate');
  $paths[] = url('courses/postgraduate');
  $paths[] = url('courses/subjects');
  $paths[] = url('course-search');

  // Add the URLs to the Acquia Purge queue.
  $service = _acquia_purge_service();
  $service->addPaths($paths);

  return $paths;
}

/**
 * Get accordion tabs which will be added on courses page.
 */
function uow_second_year_get_accordion_additional_tabs() {
  $settings                 = uow_second_year_get_settings();
  $default_accordion_tab_id = $settings['default_accordion_tab'];

  $is_next_year_ug_hidden = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES) && !empty($settings['hide_courses'][UOW_IMPORT_LEVEL_UDEGRATUATE_TID]);
  $is_next_year_pg_hidden = !user_access(UOW_SECOND_YEAR_ACCESS_HIDDEN_SECOND_YEAR_COURSES) && !empty($settings['hide_courses'][UOW_IMPORT_LEVEL_POSTGRADUATE_TID]);

  $query = db_select('field_data_field_accordion_tab', 't');
  $query->join('field_data_field_tab_title', 'fdftt',
    't.field_accordion_tab_value = fdftt.entity_id');
  $query->join('field_data_field_tab_listing', 'fdftl',
    't.field_accordion_tab_value = fdftl.entity_id');

  // Hide next year UG courses if needed.
  $hide_ug_condition = db_or()
    ->condition('fdftt.field_tab_title_value',
      '%' . db_like('[' . UOW_SECOND_YEAR_CURRENT_YEAR_STRING . ']') . '%',
      'LIKE')
    ->condition('fdftt.field_tab_title_value',
      '%' . db_like('[' . UOW_SECOND_YEAR_NEXT_YEAR_STRING . ']') . '%',
      $is_next_year_ug_hidden ? 'NOT LIKE' : 'LIKE');

  $ug_condition = db_and()
    ->condition('fdftl.field_tab_listing_vargs', array(
      UOW_IMPORT_COURSE_TYPE_FOUNDATION_COURSE,
      UOW_IMPORT_COURSE_TYPE_UDEGRATUATE
    ), 'IN')
    ->condition($hide_ug_condition);

  // Hide next year PG courses if needed.
  $hide_pg_condition = db_or()
    ->condition('fdftt.field_tab_title_value',
      '%' . db_like('[' . UOW_SECOND_YEAR_CURRENT_YEAR_STRING . ']') . '%',
      'LIKE')
    ->condition('fdftt.field_tab_title_value',
      '%' . db_like('[' . UOW_SECOND_YEAR_NEXT_YEAR_STRING . ']') . '%',
      $is_next_year_pg_hidden ? 'NOT LIKE' : 'LIKE');
  $pg_condition      = db_and()
    ->condition('fdftl.field_tab_listing_vargs',
      UOW_IMPORT_COURSE_TYPE_POSTGRADUATE)
    ->condition($hide_pg_condition);

  $query->condition(db_or()
    ->condition($ug_condition)
    ->condition($pg_condition));
  //->condition('fdftl.field_tab_listing_vargs', UOW_IMPORT_COURSE_TYPE_FOUNDATION_COURSE));

  $query->fields('t')
    ->condition('t.entity_id', $default_accordion_tab_id)
    ->orderBy('t.field_accordion_tab_value', 'DESC');
  $tabs = $query->execute()->fetchAll();

  return $tabs;
}

/**
 * Check current course type.
 */
function uow_second_year_check_course_type($node, $type = 'Undergraduate') {
  $result = FALSE;
  // Get course type from course data.
  $course_type_tid = uow_core_field_get_item('node', $node, 'field_course_type',
    0, 'target_id');
  if (!empty($course_type_tid)) {
    $course_type = taxonomy_term_load($course_type_tid);
    $course_type = $course_type->name;

    if ($course_type === $type) {
      $result = TRUE;
    }
  }

  return $result;
}

/**
 * Get params for apply button.
 */
function uow_second_year_get_apply_button_params($node) {
  $defaults                    = uow_second_year_get_settings();
  $defaults_entry_apply_button = uow_second_year_get_entry_apply_button_settings();
  $apply_button_params         = array(
    'hide_apply_button'      => !UOW_SECOND_YEAR_HIDE_BUTTON_APPLY,
    'hide_apply_button_text' => '',
  );

  // Apply course button generation.
  $apply_type = uow_core_field_get_item('node', $node, 'field_apply_type', 0,
    'value');

  if (uow_second_year_check_is_nextyear()) {
    $apply_button_params['hide_apply_button']      = $defaults['hide_apply_button_ug'];
    $apply_button_params['hide_apply_button_text'] = $defaults['hide_apply_button_ug_text'];

    if (uow_second_year_check_course_type($node, 'Postgraduate')) {
      $apply_button_params['hide_apply_button']      = $defaults['hide_apply_button_pg'];
      $apply_button_params['hide_apply_button_text'] = $defaults['hide_apply_button_pg_text'];
    }

    if ($apply_button_params['hide_apply_button'] != UOW_SECOND_YEAR_HIDE_BUTTON_APPLY
      && strstr($apply_type, 'IPP seq')
    ) {
      $apply_button_params['hide_apply_button']      = $defaults_entry_apply_button['hide_parttime_apply_button_next_year'];
      $apply_button_params['hide_apply_button_text'] = $defaults_entry_apply_button['hide_parttime_apply_button_next_year_text'];
    }
  }
  else {
    if ($apply_button_params['hide_apply_button'] != UOW_SECOND_YEAR_HIDE_BUTTON_APPLY
      && strstr($apply_type, 'IPP seq')
    ) {
      $apply_button_params['hide_apply_button']      = $defaults_entry_apply_button['hide_parttime_apply_button_current_year'];
      $apply_button_params['hide_apply_button_text'] = $defaults_entry_apply_button['hide_parttime_apply_button_current_year_text'];
    }
  }

  return $apply_button_params;
}

/**
 * Batch processing callback; Generate aliases for users.
 */
function uow_second_year_bulk_update_batch_process(&$context) {
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count']   = 0;
    $context['sandbox']['current'] = 0;
  }

  $pages       = array(
    'courses/undergraduate',
    'courses/postgraduate',
  );
  $total_pages = count($pages);

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = $total_pages;

    // If there are no nodes to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      return;
    }
  }

  // Load pathauto.module from the pathauto module.
  module_load_include('inc', 'pathauto');
  module_load_include('inc', 'pathauto.pathauto');

  foreach ($pages as $key => $alias) {
    $path = drupal_lookup_path('source', $alias);
    $node = menu_get_object('node', 1, $path);

    if ($node->nid) {
      $source_next_year = 'node/' . $node->nid . '/?next-year=1';
      $alias_next_year  = $alias . '/next-year';
      uow_aliases_create_alias($alias_next_year, $source_next_year, 'update');
    }
  }

  $context['sandbox']['count'] += $total_pages;
  $context['sandbox']['current'] = $total_pages;
  $context['message']            = t('Updated aliases for PG and UG courses pages.');

  if ($context['sandbox']['count'] != $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}
