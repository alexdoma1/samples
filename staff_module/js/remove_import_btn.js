/**
 * @file
 * Contains js for change search.
 */

(function ($) {
    Drupal.behaviors.staffRemoveBtn = {
        attach: function () {
            $(document).ready(function () {
                $('body').once().on('click', '.import-btn', function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: this.href,
                        success: function (result) {
                            $('.import-app-' + result).remove();
                        }
                    });

                });
            });
        }
    };
})(jQuery);
