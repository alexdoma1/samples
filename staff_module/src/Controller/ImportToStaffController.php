<?php

namespace Drupal\staff_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;

class ImportToStaffController extends ControllerBase {

  public function import($nid) {
    $result = '';
    $account = \Drupal::currentUser();


    $applicant = Node::load($nid);

    $staff = Node::create([
      // The node entity bundle.
      'type' => 'staff',
      'langcode' => 'en',
      'uid' => $account->id(),
      'moderation_state' => 'published',
      'title' => $applicant->getTitle(),
    ]);
    if (!empty($applicant->body->value)) {
      $staff->set('field_staff_description', $applicant->body->getValue());
    }
    if (!empty($applicant->field_ap_birthday->value)) {
      $dateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $applicant->field_ap_birthday->value);
      $newDateString = $dateTime->format('Y-m-d');
      $staff->set('field_staff_birthday', $newDateString);
    }
    if (!empty($applicant->field_ap_email->value)) {
      $staff->set('field_staff_email', $applicant->field_ap_email->value);
    }
    if (!empty($applicant->field_ap_phone->value)) {
      $staff->set('field_staff_phone', $applicant->field_ap_phone->value);
    }
    if (!empty($applicant->field_ap_skype->value)) {
      $staff->set('field_staff_skype', $applicant->field_ap_skype->value);
    }
    if (!empty($applicant->field_ap_additional_links->getValue())) {
      $links = $applicant->field_ap_additional_links->getValue();
      foreach ($links as $link) {
        $staff->field_staff_accounts->appendItem($link['value']);
      }
    }
    if (!empty($applicant->field_ap_cv->getValue())) {
      $files = $applicant->field_ap_cv->getValue();
      foreach ($files as $file) {
        $staff->field_staff_cv->appendItem($file);
      }
    }
    $staff->save();

    if ($staff->id()) {
      $applicant->set('field_imported_to_staff', '1');
      $applicant->save();
      $result = $applicant->id();
    }

    return new JsonResponse($result);

  }
}
