(function($, Drupal) {

  if(Drupal.AjaxCommands){

    // Custom Ajax command
    Drupal.AjaxCommands.prototype.commentsHistoryAjaxCommand = function(ajax, response, status){
      var comment = $('.history-comments-' + response.nid);
      comment.html(response.view);
    }
  }
})(jQuery, Drupal);