<?php

namespace Drupal\da_comment\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class CommentController.
 */
class CommentController extends ControllerBase {

  public function addComment($method, $nid) {
    $prepared_form_values = [
      'node_id' => $nid,
    ];

    $add_comment_form
      = \Drupal::formBuilder()->getForm('Drupal\da_comment\Form\AddCommentForm', $prepared_form_values);

    return array(
      '#type' => 'markup',
      '#markup' => render($add_comment_form),
    );
  }

}
