<?php

namespace Drupal\da_comment\Ajax;
use Drupal\Core\Ajax\CommandInterface;
use Drupal\views\Views;

class CommentsHistoryCommand implements CommandInterface {

  protected $node_id;
  protected $view_name;
  # Constructs
  public function __construct($node_id, $view_name) {
    $this->node_id = $node_id;
    $this->view_name = $view_name;
  }

  public function render() {
    $view = Views::getView($this->view_name);
    if (is_object($view)) {
      $view->setArguments([$this->node_id]);
      $last_message = $view->render();
    }
    return array(
      'command' => 'commentsHistoryAjaxCommand',
      'view' => render($last_message),
      'nid' => $this->node_id,
    );
  }
}
