<?php

namespace Drupal\da_comment\Form;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\da_comment\Ajax\CommentsHistoryCommand;

/**
 * Class AddCommentForm.
 */
class AddCommentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_comment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $prepared_form_values = NULL) {

    $form['node_id'] = [
      '#type' => 'hidden',
      '#value' => $prepared_form_values['node_id'],
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#default_value' => '',
      '#required' => TRUE,
      '#attribures' => [
        'placeholder' => $this->t('Enter your message ...'),
      ],
      '#weight' => 0,
    ];

    $form['system_messages'] = [
      '#markup' => '<div id="form-system-messages"></div>',
      '#weight' => -101,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => 'Submit',
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate the body field.
    $body = trim($form_state->getValue('body'));
    if (empty($body)) {
      $form_state->setErrorByName(
        'node_id', t("This is a required field")
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $account = \Drupal::currentUser();

    $nid = $form_state->getValue('node_id');
    $subject = mb_substr($form_state->getValue('body'), 0, 32);

    $values = [
      'entity_type' => 'node',
      'entity_id' => $nid,
      'field_name' => da_comment_get_field_by_type($nid),
      // The user id of the comment's 'author'. Use 0 for the anonymous user.
      'uid' => $account->id(),

      // These values are for the comment itself.
      'comment_type' => 'comment',
      'subject' => $subject,
      'comment_body' => $form_state->getValue('body'),
      'status' => 1,
    ];

    $comment = Comment::create($values);
    $comment->save();

    drupal_set_message('Comment was added!');
  }

  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    $this->handleAjaxSubmit($form_state, $ajax_response);

    $message = [
      '#type' => 'status_messages',
    ];
    $messages = \Drupal::service('renderer')->renderRoot($message);

    $ajax_response->addCommand(new HtmlCommand('#form-system-messages', $messages));

    return $ajax_response;
  }

  /**
   * Handles ajax request.
   */
  protected function handleAjaxSubmit(FormStateInterface $form_state, AjaxResponse $ajax_response) {
    $errors = \Drupal::messenger()->messagesByType('error');
    if (!empty($errors)) {
      return;
    }
    $node_id = $form_state->getValue('node_id');

    $view_name = da_comments_get_comment_view_by_id($node_id);
    $message = [
      '#theme' => 'status_messages',
      '#message_list' => drupal_get_messages(),
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];
    $messages = \Drupal::service('renderer')->render($message);
    $ajax_response->addCommand(new CloseModalDialogCommand());
    $ajax_response->addCommand(new CommentsHistoryCommand($node_id, $view_name));
    if (!empty($messages)) {
      $ajax_response->addCommand(new PrependCommand('.history-comments-' . $node_id, $messages));
    }
  }
}